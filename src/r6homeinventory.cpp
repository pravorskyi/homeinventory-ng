/*
 * r6homeinventory,cpp - Main window class
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "r6homeinventory.h"

R6HomeInventory::R6HomeInventory(QWidget *parent, Qt::WindowFlags flags)
    : QMainWindow(parent, flags)
{
	// Initialize the SystemLayer
	currenttitle = qApp->applicationName() + " 3.0-alpha";
	currentsection = "";
	currentsystemlayer = 0;
	currentbusinesslayer = 0;
	currentitem = 0;
	currentcategory = 0;
	currentlocation = 0;
	categoryroot = 0;
	locationroot = 0;

	currentsystemlayer = new SystemLayer();
	currentbusinesslayer = new BusinessLayer(currentsystemlayer);

	ui.setupUi(this);

	// Set Window Title
	this->setWindowTitle(currenttitle);

	ui.actionAbout_R6_Home_Inventory->setText(tr("About ") + qApp->applicationName());

	// Set the item column headers
	QStringList itemColumnHeaders;
	itemColumnHeaders << "Name" << "Manufacturer" << "Category" << "Location" << "Item Value";	
	ui.itemsTreeWidget->setHeaderLabels(itemColumnHeaders);

	QHeaderView* header = ui.itemsTreeWidget->header();
    header->setSectionResizeMode(QHeaderView::Stretch);
	ui.itemsTreeWidget->setHeader(header);

	// Set the sections column header
	QStringList sectionsColumnHeaders;
	sectionsColumnHeaders << "Sections";	
	ui.sectionsTreeWidget->setHeaderLabels(sectionsColumnHeaders);

	this->showMaximized();

    this->disableControls(); 
	this->loadRecentFile();

	this->LoadData();
}

R6HomeInventory::~R6HomeInventory()
{

}

void R6HomeInventory::disableControls()
{
	ui.sectionsTreeWidget->setEnabled(false);
	ui.itemsTreeWidget->setEnabled(false);
	ui.detailTextBrowser->setEnabled(false);
	ui.actionAll_Items->setEnabled(false);
	ui.actionReports->setEnabled(false);
	ui.actionAddItem->setEnabled(false);
	ui.actionEditItem->setEnabled(false);
	ui.actionDeleteItem->setEnabled(false);
	ui.actionAddCategory->setEnabled(false);
	ui.actionEditCategory->setEnabled(false);
	ui.actionDeleteCategory->setEnabled(false);
	ui.actionAddLocation->setEnabled(false);
	ui.actionEditLocation->setEnabled(false);
	ui.actionDeleteLocation->setEnabled(false);
}

void R6HomeInventory::enableControls()
{
	ui.sectionsTreeWidget->setEnabled(true);
	ui.itemsTreeWidget->setEnabled(true);
	ui.detailTextBrowser->setEnabled(true);
	ui.actionAll_Items->setEnabled(true);
	ui.actionReports->setEnabled(true);
	ui.actionAddItem->setEnabled(true);
	ui.actionEditItem->setEnabled(true);
	ui.actionDeleteItem->setEnabled(true);
	ui.actionAddCategory->setEnabled(true);
	ui.actionEditCategory->setEnabled(true);
	ui.actionDeleteCategory->setEnabled(true);
	ui.actionAddLocation->setEnabled(true);
	ui.actionEditLocation->setEnabled(true);
	ui.actionDeleteLocation->setEnabled(true);
}

void R6HomeInventory::loadRecentFile()
{
	if(currentsystemlayer->getLastOpenedFile() != "")
	{
		if(this->currentbusinesslayer->CheckFile(currentsystemlayer->getLastOpenedFile()))
		{
			this->currentbusinesslayer->LoadData();
			ui.statusBar->showMessage(tr("Loading data"), 2000);

			LoadData();			

			this->enableControls();

			this->setWindowTitle(currenttitle + " - " + currentsystemlayer->getLastOpenedFile());
		}
	}
}

void R6HomeInventory::LoadData()
{
	LoadSections();
	LoadItems();
}

void R6HomeInventory::LoadSections()
{
	currentcategory = 0;
	currentlocation = 0;

	ui.sectionsTreeWidget->clear();
	
	categoryroot = new QTreeWidgetItem((QTreeWidget*)0, QStringList(QString("Categories")));
	categoryroot->setIcon(0, QIcon(QString::fromUtf8(":/R6HomeInventory/Resources/category.png")));
	categoryroot->setData(0, Qt::UserRole, "Categories");

	locationroot = new QTreeWidgetItem((QTreeWidget*)0, QStringList(QString("Locations")));
	locationroot->setIcon(0, QIcon(QString::fromUtf8(":/R6HomeInventory/Resources/location.png")));
	locationroot->setData(0, Qt::UserRole, "Locations");

	// Populate category treeview items
	ui.sectionsTreeWidget->addTopLevelItem(categoryroot);
	
	Category *category = 0;
	QTreeWidgetItem *categoryitem = 0;

	QHashIterator<int, Category*> categoryinterator(this->currentbusinesslayer->categories);
	
    while(categoryinterator.hasNext()) 
	{
        categoryinterator.next();
        category = categoryinterator.value();						

		categoryitem = new QTreeWidgetItem(QStringList(QString(category->getName())));
		categoryitem->setIcon(0, QIcon(QString::fromUtf8(":/R6HomeInventory/Resources/category.png")));
		categoryitem->setData(0, Qt::UserRole, category->getID());

		categoryroot->addChild(categoryitem);	
    }	

	// Populate location treeview items
	ui.sectionsTreeWidget->addTopLevelItem(locationroot);

	Location *location = 0;
	QTreeWidgetItem *locationitem = 0;

	QHashIterator<int, Location*> locationinterator(this->currentbusinesslayer->locations);
	
    while(locationinterator.hasNext()) 
	{
        locationinterator.next();
        location = locationinterator.value();						

		locationitem = new QTreeWidgetItem(QStringList(QString(location->getName())));
		locationitem->setIcon(0, QIcon(QString::fromUtf8(":/R6HomeInventory/Resources/location.png")));
		locationitem->setData(0, Qt::UserRole, location->getID());

		locationroot->addChild(locationitem);
    }	

	ui.sectionsTreeWidget->expandAll();
}

void R6HomeInventory::LoadItems()
{
	currentitem = 0;

	ui.itemsTreeWidget->clear();
	ui.detailTextBrowser->setHtml("");

	// Populate items
	Item *item = 0;
	QTreeWidgetItem *treeitem = 0;

	QHashIterator<int, Item*> iteminterator(this->currentbusinesslayer->items);
	
    while(iteminterator.hasNext()) 
	{
        iteminterator.next();
        item = iteminterator.value();						

		QStringList itemdata;		
		itemdata << item->getName() << item->getManufacturer() << item->getCategory()->getName() << item->getLocation()->getName() << this->currentbusinesslayer->toCurrencyString(item->getItemValue());

		treeitem = new QTreeWidgetItem(itemdata);
		treeitem->setTextAlignment(0, Qt::AlignHCenter);
		treeitem->setTextAlignment(1, Qt::AlignHCenter);
		treeitem->setTextAlignment(2, Qt::AlignHCenter);
		treeitem->setTextAlignment(3, Qt::AlignHCenter);
		treeitem->setTextAlignment(4, Qt::AlignHCenter);
		treeitem->setIcon(0, QIcon(QString::fromUtf8(":/R6HomeInventory/Resources/item.png")));
		treeitem->setData(0, Qt::UserRole, item->getID());

		ui.itemsTreeWidget->addTopLevelItem(treeitem);
    }	
}

void R6HomeInventory::LoadItems(Category *category)
{
	currentitem = 0;

	ui.itemsTreeWidget->clear();
	ui.detailTextBrowser->setHtml("");

	// Populate items
	Item *item = 0;
	QTreeWidgetItem *treeitem = 0;

	QHashIterator<int, Item*> iteminterator(this->currentbusinesslayer->items);
	
    while(iteminterator.hasNext()) 
	{
        iteminterator.next();
        item = iteminterator.value();						

		if(item->getCategory()->getID() == category->getID())
		{
			QStringList itemdata;		
			itemdata << item->getName() << item->getManufacturer() << item->getCategory()->getName() << item->getLocation()->getName() << this->currentbusinesslayer->toCurrencyString(item->getItemValue());

			treeitem = new QTreeWidgetItem(itemdata);
			treeitem->setTextAlignment(0, Qt::AlignHCenter);
			treeitem->setTextAlignment(1, Qt::AlignHCenter);
			treeitem->setTextAlignment(2, Qt::AlignHCenter);
			treeitem->setTextAlignment(3, Qt::AlignHCenter);
			treeitem->setTextAlignment(4, Qt::AlignHCenter);
			treeitem->setIcon(0, QIcon(QString::fromUtf8(":/R6HomeInventory/Resources/item.png")));
			treeitem->setData(0, Qt::UserRole, item->getID());

			ui.itemsTreeWidget->addTopLevelItem(treeitem);
		}
    }	
}

void R6HomeInventory::LoadItems(Location *location)
{
	currentitem = 0;

	ui.itemsTreeWidget->clear();
	ui.detailTextBrowser->setHtml("");

	// Populate items
	Item *item = 0;
	QTreeWidgetItem *treeitem = 0;

	QHashIterator<int, Item*> iteminterator(this->currentbusinesslayer->items);
	
    while(iteminterator.hasNext()) 
	{
        iteminterator.next();
        item = iteminterator.value();						

		if(item->getLocation()->getID() == location->getID())
		{
			QStringList itemdata;		
			itemdata << item->getName() << item->getManufacturer() << item->getCategory()->getName() << item->getLocation()->getName() << this->currentbusinesslayer->toCurrencyString(item->getItemValue());

			treeitem = new QTreeWidgetItem(itemdata);
			treeitem->setTextAlignment(0, Qt::AlignHCenter);
			treeitem->setTextAlignment(1, Qt::AlignHCenter);
			treeitem->setTextAlignment(2, Qt::AlignHCenter);
			treeitem->setTextAlignment(3, Qt::AlignHCenter);
			treeitem->setTextAlignment(4, Qt::AlignHCenter);
			treeitem->setIcon(0, QIcon(QString::fromUtf8(":/R6HomeInventory/Resources/item.png")));
			treeitem->setData(0, Qt::UserRole, item->getID());

			ui.itemsTreeWidget->addTopLevelItem(treeitem);
		}
    }	
}

void R6HomeInventory::getSectionSelected()
{
	QList<QTreeWidgetItem *> selecteditems = ui.sectionsTreeWidget->selectedItems();

	foreach(QTreeWidgetItem *item, selecteditems)
	{
		if(item)
		{
			if(item->parent())
			{
				if(item->parent()->data(0, Qt::UserRole).toString() == "Categories")
				{
					currentcategory = this->currentbusinesslayer->categories[item->data(0, Qt::UserRole).toInt()];			
					this->LoadItems(currentcategory);
					currentsection = "category";
				}

				if(item->parent()->data(0, Qt::UserRole).toString() == "Locations")
				{
					currentlocation = this->currentbusinesslayer->locations[item->data(0, Qt::UserRole).toInt()];			
					this->LoadItems(currentlocation);
					currentsection = "location";
				}
			}
			else
			{
				if(item->data(0, Qt::UserRole).toString() == "Categories")
				{
					this->LoadItems();
					currentsection = "categoryroot";
				}

				if(item->data(0, Qt::UserRole).toString() == "Locations")
				{
					this->LoadItems();
					currentsection = "locationroot";
				}
			}
		}				
	}
}

void R6HomeInventory::setSectionSelected()
{
	/*qDebug(currentsection.toAscii());
	if(currentsection == "category")
	{
		this->LoadItems(currentcategory);
	}

	if(currentsection == "location")
	{
		this->LoadItems(currentlocation);
	}*/
}

void R6HomeInventory::getItemSelected()
{
	QList<QTreeWidgetItem *> selecteditems = ui.itemsTreeWidget->selectedItems();

	foreach(QTreeWidgetItem *item, selecteditems)
	{
		if(item)
		{
			currentitem = this->currentbusinesslayer->items[item->data(0, Qt::UserRole).toInt()];			
		}				
	}
}

void R6HomeInventory::on_actionNew_triggered()
{
	// Do not allow someone to use NewFile to open an existing file

	QString fileName;
	QStringList fileNames;
	
	QFileDialog *fd = new QFileDialog(this, "Create a new file...", "c:\\", "R6 Home Inventory (*.r6hi)");
    fd->setFileMode(QFileDialog::AnyFile);	
	fd->setAcceptMode(QFileDialog::AcceptSave);
	
	if (fd->exec() == QDialog::Accepted)
	{
        fileNames = fd->selectedFiles();

		QStringList::const_iterator i;
		
		for (i = fileNames.constBegin(); i != fileNames.constEnd(); ++i)
		{
			fileName = (*i);
		}		

		// Check to see if the file does not have an extension, then append one
		if(!fileName.endsWith(".r6hi"))
		{
			fileName += ".r6hi";
		}

		// Check to see if the file exists, if so alert the user
		if(QFile::exists(fileName))
		{
			QMessageBox::critical(this, "File Create Error",
			"You cannot overwrite an existing file.\n"
			"Please use a different file name.");

			return;
		}
		
		if(this->currentbusinesslayer->CheckFile(fileName))
		{
			this->currentbusinesslayer->LoadData();
			ui.statusBar->showMessage(tr("Loading data"), 2000);
			LoadData();			
			this->enableControls();
			this->currentsystemlayer->setLastOpenedFile(fileName);
			this->setWindowTitle(currenttitle + " - " + fileName);
		}
	}	

	delete fd;
}

void R6HomeInventory::on_actionOpen_triggered()
{
	QString fileName;
	QStringList fileNames;
	
	QFileDialog *fd = new QFileDialog(this, "Open an existing file...", "c:\\", "R6 Home Inventory (*.r6hi)");
    fd->setFileMode(QFileDialog::ExistingFile);	
	
	if (fd->exec() == QDialog::Accepted)
	{
        fileNames = fd->selectedFiles();

		QStringList::const_iterator i;
		
		for (i = fileNames.constBegin(); i != fileNames.constEnd(); ++i)
		{
			fileName = (*i);
		}

		if(this->currentbusinesslayer->CheckFile(fileName))
		{
			this->currentbusinesslayer->LoadData();
			ui.statusBar->showMessage(tr("Loading data"), 2000);
			LoadData();			
			this->enableControls();
			this->currentsystemlayer->setLastOpenedFile(fileName);
			this->setWindowTitle(currenttitle + " - " + fileName);
		}
	}	
	
	delete fd;
}

void R6HomeInventory::on_actionExit_triggered()
{
	this->close();
}

void R6HomeInventory::on_actionAddCategory_triggered()
{
	CategoryDialog categorydialog(this, 0, this->currentbusinesslayer);
	categorydialog.exec();
	LoadData();
	setSectionSelected();
}

void R6HomeInventory::on_actionAddLocation_triggered()
{
	LocationDialog locationdialog(this, 0, this->currentbusinesslayer);
	locationdialog.exec();
	LoadData();
	setSectionSelected();
}

void R6HomeInventory::on_actionAddItem_triggered()
{
	ItemDialog itemdialog(this, 0, this->currentbusinesslayer);
	
	 if(currentsection == "category" && currentcategory)
	{
		itemdialog.setCurrentCategory(currentcategory->getID());
	}
	if(currentsection == "location" && currentlocation)
	{
		itemdialog.setCurrentLocation(currentlocation->getID());
	}
	
	itemdialog.exec();
	LoadItems();
	setSectionSelected();
}

void R6HomeInventory::on_actionEditCategory_triggered()
{
	if(currentcategory)
	{
		CategoryDialog categorydialog(this, 0, this->currentbusinesslayer);
		categorydialog.setCurrentCategory(currentcategory);	
		categorydialog.exec();
		LoadData();
		setSectionSelected();
	}
}

void R6HomeInventory::on_actionEditLocation_triggered()
{
	if(currentlocation)
	{
		LocationDialog locationdialog(this, 0, this->currentbusinesslayer);
		locationdialog.setCurrentLocation(currentlocation);	
		locationdialog.exec();
		LoadData();
		setSectionSelected();
	}
}

void R6HomeInventory::on_actionEditItem_triggered()
{
	if(currentitem)
	{
		ItemDialog itemdialog(this, 0, this->currentbusinesslayer);
		itemdialog.setCurrentItem(currentitem);	
		itemdialog.exec();
		LoadItems();
		this->setSectionSelected();
	}
}

void R6HomeInventory::on_actionDeleteItem_triggered()
{
	if(currentitem)
	{
		this->currentbusinesslayer->Delete(currentitem);
		LoadItems();
		setSectionSelected();
	}
}

void R6HomeInventory::on_actionDeleteCategory_triggered()
{
	if(currentcategory)
	{
		if(this->currentbusinesslayer->hasItems(currentcategory))
		{
			MoveItemsDialog moveitemsdialog;
			int result = 0;

			// Show Dialog
			QMessageBox categorydeletemb("Category Delete",
				"You are attempting to delete a category with items.\n"
				"Please select an option:",
				QMessageBox::Warning,
				QMessageBox::Yes | QMessageBox::Default,
				QMessageBox::No, 0);
			categorydeletemb.setWindowIcon(QIcon(QString::fromUtf8(":/R6HomeInventory/Resources/category.png")));
            categorydeletemb.setButtonText(QMessageBox::Yes, "Move Items");
            categorydeletemb.setButtonText(QMessageBox::No, "Delete Items");

            switch(categorydeletemb.exec()) 
			{
				case QMessageBox::Yes:
					// Move items
					moveitemsdialog.setCurrentBusinessLayer(this->currentbusinesslayer);
					moveitemsdialog.setSectionType("category");
					moveitemsdialog.setWindowIcon(QIcon(QString::fromUtf8(":/R6HomeInventory/Resources/category.png")));
					result = moveitemsdialog.exec();
					
					if(result == 0)
					{
						break;
					}

					if(currentcategory->getID() == moveitemsdialog.getMoveTo())
					{
						QMessageBox::critical(this, "Category Delete Error",
							"You cannot move items to the \n"
							"category you are deleting.");	
						break;
					}
					else
					{
						this->currentbusinesslayer->moveItems(currentcategory, this->currentbusinesslayer->categories[moveitemsdialog.getMoveTo()]);
						this->currentbusinesslayer->Delete(currentcategory);
					}
					
					break;
				case QMessageBox::No:
					// Delete items
					
					this->currentbusinesslayer->deleteItems(currentcategory);
					this->currentbusinesslayer->Delete(currentcategory);

					break;				
            }
		}
		else
		{
			this->currentbusinesslayer->Delete(currentcategory);
		}
		
		currentsection = "";
		currentcategory = 0;

		LoadData();
		setSectionSelected();
	}
}

void R6HomeInventory::on_actionDeleteLocation_triggered()
{
	if(currentlocation)
	{
		if(this->currentbusinesslayer->hasItems(currentlocation))
		{
			MoveItemsDialog moveitemsdialog;
			int result = 0;

			// Show Dialog
			QMessageBox locationdeletemb("Location Delete",
				"You are attempting to delete a location with items.\n"
				"Please select an option:",
				QMessageBox::Warning,
				QMessageBox::Yes | QMessageBox::Default,
				QMessageBox::No, 0);
			locationdeletemb.setWindowIcon(QIcon(QString::fromUtf8(":/R6HomeInventory/Resources/location.png")));
            locationdeletemb.setButtonText(QMessageBox::Yes, "Move Items");
            locationdeletemb.setButtonText(QMessageBox::No, "Delete Items");

            switch(locationdeletemb.exec()) 
			{
				case QMessageBox::Yes:
					// Move items
					moveitemsdialog.setCurrentBusinessLayer(this->currentbusinesslayer);
					moveitemsdialog.setSectionType("location");
					moveitemsdialog.setWindowIcon(QIcon(QString::fromUtf8(":/R6HomeInventory/Resources/location.png")));
					result = moveitemsdialog.exec();

					if(result == 0)
					{
						break;
					}

					if(currentlocation->getID() == moveitemsdialog.getMoveTo())
					{
						QMessageBox::critical(this, "Location Delete Error",
							"You cannot move items to the \n"
							"location you are deleting.");	
						break;
					}
					else
					{
						this->currentbusinesslayer->moveItems(currentlocation, this->currentbusinesslayer->locations[moveitemsdialog.getMoveTo()]);
						this->currentbusinesslayer->Delete(currentlocation);
					}
					
					break;
				case QMessageBox::No:
					// Delete items
					
					this->currentbusinesslayer->deleteItems(currentlocation);
					this->currentbusinesslayer->Delete(currentlocation);

					break;				
            }
		}
		else
		{
			this->currentbusinesslayer->Delete(currentlocation);
		}
		
		currentsection = "";
		currentlocation = 0;

		LoadData();
		setSectionSelected();
	}
}

void R6HomeInventory::on_sectionsTreeWidget_itemClicked(QTreeWidgetItem*,int)
{
	this->getSectionSelected();
}

void R6HomeInventory::on_itemsTreeWidget_itemClicked(QTreeWidgetItem*,int)
{
	this->getItemSelected();

	// Set detail
	if(currentitem)
	{
		QString itemdetail = "";
		QString imagesrc = "";
		QString fontsize = "4";

		itemdetail += "<html><body><center><table width='100%' border='1' cellspacing='0' cellpadding='2' bgcolor='black'><tr>";
		itemdetail += "<td bgcolor='white'><table width='100%' cellpadding='2' cellspacing='0'>";
		itemdetail += "<tr bgcolor='lightgrey'><td><font size='" + fontsize + "'><b>Item Name:</b></font></td><td><font size='" + fontsize + "'>" + currentitem->getName() + "</font></td></tr>";
		itemdetail += "<tr bgcolor='white'><td><font size='" + fontsize + "'><b>Manufacturer:</b></font></td><td><font size='" + fontsize + "'>" + currentitem->getManufacturer() + "</font></td></tr>";
		itemdetail += "<tr bgcolor='lightgrey'><td><font size='" + fontsize + "'><b>Model:</b></font></td><td><font size='" + fontsize + "'>" + currentitem->getModel() + "</font></td></tr>";
		itemdetail += "<tr bgcolor='white'><td><font size='" + fontsize + "'><b>Category:</b></font></td><td><font size='" + fontsize + "'>" + currentitem->getCategory()->getName() + "</font></td></tr>";
		itemdetail += "<tr bgcolor='lightgrey'><td><font size='" + fontsize + "'><b>Location:</b></font></td><td><font size='" + fontsize + "'>" + currentitem->getLocation()->getName() + "</font></td></tr>";
		itemdetail += "<tr bgcolor='white'><td><font size='" + fontsize + "'><b>Serial Number:</b></font></td><td><font size='" + fontsize + "'>" + currentitem->getSerialNumber() + "</font></td></tr>";
		itemdetail += "<tr bgcolor='lightgrey'><td><font size='" + fontsize + "'><b>Date Acquired:</b></font></td><td><font size='" + fontsize + "'>" + currentitem->getDateAcquired().toString("MM/dd/yyyy") + "</font></td></tr>";
		itemdetail += "<tr bgcolor='white'><td><font size='" + fontsize + "'><b>Quantity:</b></font></td><td><font size='" + fontsize + "'>" + QVariant(currentitem->getQuantity()).toString() + "</font></td></tr>";
		itemdetail += "<tr bgcolor='lightgrey'><td><font size='" + fontsize + "'><b>Item Value:</b></font></td><td><font size='" + fontsize + "'>" + this->currentbusinesslayer->toCurrencyString(currentitem->getItemValue()) + "</font></td></tr>";
		itemdetail += "</table></td>";

		
		if(currentitem->getImageLocation() != "")
		{
			QImage itemimage(currentitem->getImageLocation());
			QSize imagesize = currentbusinesslayer->scaleImageSize(itemimage.width(), itemimage.height(), (ui.detailTextBrowser->width() * .75), (ui.detailTextBrowser->height() * .75));
			itemdetail += "<td bgcolor='white' align='right'><img src='" + currentitem->getImageLocation() + "' width='" + QVariant(imagesize.width()).toString() + "' height='" + QVariant(imagesize.height()).toString() + "'></td>";
		}
		else
		{
			itemdetail += "<td bgcolor='white' align='center' valign='center'><br><br><b>No image.</b></td>";
		}

		itemdetail += "</tr></table></center></body></html>";

		ui.detailTextBrowser->setHtml(itemdetail);
	}
}

void R6HomeInventory::on_sectionsTreeWidget_itemDoubleClicked(QTreeWidgetItem* item,int)
{
	this->getSectionSelected();

	if(item)
	{
		if(item->parent())
		{
			if(item->parent()->data(0, Qt::UserRole).toString() == "Categories")
			{
				if(currentcategory)
				{
					CategoryDialog categorydialog(this, 0, this->currentbusinesslayer);
					categorydialog.setCurrentCategory(currentcategory);	
					categorydialog.exec();
					LoadData();
					this->setSectionSelected();
					return;
				}
			}

			if(item->parent()->data(0, Qt::UserRole).toString() == "Locations")
			{
				if(currentlocation)
				{
					LocationDialog locationdialog(this, 0, this->currentbusinesslayer);
					locationdialog.setCurrentLocation(currentlocation);	
					locationdialog.exec();
					LoadData();
					this->setSectionSelected();
					return;
				}
			}
		}
	}
}

void R6HomeInventory::on_itemsTreeWidget_itemDoubleClicked(QTreeWidgetItem *item,int)
{
	this->getItemSelected();

	if(item)
	{
		if(currentitem)
		{
			ItemDialog itemdialog(this, 0, this->currentbusinesslayer);
			itemdialog.setCurrentItem(currentitem);
			itemdialog.exec();
			LoadItems();
			//this->setSectionSelected();
		}
	
	}
}

void R6HomeInventory::on_actionReports_triggered()
{
	ReportDialog reportdialog(this, 0, this->currentbusinesslayer);
	reportdialog.showMaximized();
	reportdialog.exec();
}

void R6HomeInventory::on_actionAll_Items_triggered()
{
	this->LoadItems();
	currentsection = "";
}

void R6HomeInventory::on_sectionsTreeWidget_customContextMenuRequested(const QPoint & pos)
{
	QMenu menu(this);
	
	if(currentsection == "category")
	{
		menu.addAction(ui.actionAddItem);
		menu.addAction(ui.actionAddCategory);
		menu.addAction(ui.actionEditCategory);
		menu.addAction(ui.actionDeleteCategory);
	}
	if(currentsection == "categoryroot")
	{
		menu.addAction(ui.actionAddCategory);
	}
	if(currentsection == "location")
	{
		menu.addAction(ui.actionAddItem);
		menu.addAction(ui.actionAddLocation);
		menu.addAction(ui.actionEditLocation);
		menu.addAction(ui.actionDeleteLocation);
	}
	if(currentsection == "locationroot")
	{
		menu.addAction(ui.actionAddLocation);
	}

	menu.exec(ui.sectionsTreeWidget->viewport()->mapToGlobal(pos));
}

void R6HomeInventory::on_itemsTreeWidget_customContextMenuRequested(const QPoint & pos)
{
	QMenu menu(this);	

	menu.addAction(ui.actionAddItem);
	menu.addAction(ui.actionEditItem);
	menu.addAction(ui.actionDeleteItem);

	menu.exec(ui.itemsTreeWidget->viewport()->mapToGlobal(pos));
}

void R6HomeInventory::on_actionAbout_R6_Home_Inventory_triggered()
{
	AboutDialog aboutdialog;
	aboutdialog.exec();
}

void R6HomeInventory::on_actionHelp_Contents_triggered()
{
    qDebug("R6HomeInventory::on_actionHelp_Contents_triggered - NOT IMPLEMENTED.");
}
