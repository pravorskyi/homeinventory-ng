/*
 * ReportDialog.cpp - Report dialog
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "ReportDialog.h"

ReportDialog::ReportDialog(QWidget *parent, Qt::WindowFlags flags, BusinessLayer *businesslayer)
    : QDialog(parent, flags)
{
	currentbusinesslayer = businesslayer;
	ui.setupUi(this);	

	// Set the reports column header
	QStringList reportsColumnHeaders;
	reportsColumnHeaders << "Reports";	
	ui.reportsTreeWidget->setHeaderLabels(reportsColumnHeaders);

	// Configure printer
	printer.setFullPage(true);

	ui.reportTextBrowser->setHtml("<html><body><center><br><br><br><br><h1>Please select a report</h1></center></body></html>");

	this->LoadReports();
}

ReportDialog::~ReportDialog()
{

}

void ReportDialog::on_saveButton_clicked()
{
}

void ReportDialog::on_cancelButton_clicked()
{
	this->close();
}

void ReportDialog::GetReportSelected()
{
	QList<QTreeWidgetItem *> selecteditems = ui.reportsTreeWidget->selectedItems();

	foreach(QTreeWidgetItem *item, selecteditems)
	{
		if(item)
		{
			currentreport = item->data(0, Qt::UserRole).toString();
		}
	}
}

void ReportDialog::LoadReports()
{
	QTreeWidgetItem *reporttreeitem = 0;

	reporttreeitem = new QTreeWidgetItem((QTreeWidget*)0, QStringList(QString("Item List")));
	reporttreeitem->setIcon(0, QIcon(QString::fromUtf8(":/R6HomeInventory/Resources/report.png")));
	reporttreeitem->setData(0, Qt::UserRole, "ItemList");
	ui.reportsTreeWidget->addTopLevelItem(reporttreeitem);

	reporttreeitem = new QTreeWidgetItem((QTreeWidget*)0, QStringList(QString("Full Item List")));
	reporttreeitem->setIcon(0, QIcon(QString::fromUtf8(":/R6HomeInventory/Resources/report.png")));
	reporttreeitem->setData(0, Qt::UserRole, "FullItemList");
	ui.reportsTreeWidget->addTopLevelItem(reporttreeitem);

	reporttreeitem = new QTreeWidgetItem((QTreeWidget*)0, QStringList(QString("Item List with Image")));
	reporttreeitem->setIcon(0, QIcon(QString::fromUtf8(":/R6HomeInventory/Resources/report.png")));
	reporttreeitem->setData(0, Qt::UserRole, "ItemListWithImage");
	ui.reportsTreeWidget->addTopLevelItem(reporttreeitem);
	
	reporttreeitem = new QTreeWidgetItem((QTreeWidget*)0, QStringList(QString("Items By Category")));
	reporttreeitem->setIcon(0, QIcon(QString::fromUtf8(":/R6HomeInventory/Resources/report.png")));
	reporttreeitem->setData(0, Qt::UserRole, "ItemsByCategory");
	ui.reportsTreeWidget->addTopLevelItem(reporttreeitem);

	reporttreeitem = new QTreeWidgetItem((QTreeWidget*)0, QStringList(QString("Items By Location")));
	reporttreeitem->setIcon(0, QIcon(QString::fromUtf8(":/R6HomeInventory/Resources/report.png")));
	reporttreeitem->setData(0, Qt::UserRole, "ItemsByLocation");
	ui.reportsTreeWidget->addTopLevelItem(reporttreeitem);
}

void ReportDialog::LoadReport(QString reportname)
{
	ui.reportTextBrowser->setText("");

	if(currentreport == "ItemList")
	{
		this->ReportItemList();
	}
	
	if(currentreport == "FullItemList")
	{
		this->ReportFullItemList();
	}

	if(currentreport == "ItemListWithImage")
	{
		this->ReportItemListWithImage();
	}

	if(currentreport == "ItemsByCategory")
	{
		this->ReportItemsByCategory();
	}

	if(currentreport == "ItemsByLocation")
	{
		this->ReportItemsByLocation();
	}

	ui.reportTextBrowser->setHtml(currentreporthtml);
}

void ReportDialog::ReportItemList()
{
	currentreporthtml = "";
	Item *currentitem = 0;
	QString rowcolor = "LightGrey";
	
	// Populate items
	Item *item = 0;
	QTreeWidgetItem *treeitem = 0;

	QHashIterator<int, Item*> iteminterator(this->currentbusinesslayer->items);
	QHashIterator<int, Item*> itemvalueinterator(this->currentbusinesslayer->items);

	currentreporthtml += "<html><head><title>Item List</title></head><body><center><table width='100%'>";

	// Create Summary
	currentreporthtml += "<tr><td><table width='100%' cellspacing='0' cellpadding='2'>";
	currentreporthtml += "<tr><td><b><u>Report Summary</u></b></td><td></td></tr>";

	// Report title
	currentreporthtml += "<tr><td><b>Report:</b></td><td>Item List</td></tr>";
	
	// Report Date
	currentreporthtml += "<tr><td><b>Report Date:</b></td><td>" + QDate::currentDate().toString() + "</td></tr>";

	// Total Items
	currentreporthtml += "<tr><td><b>Total Items:</b></td><td>" + QVariant(this->currentbusinesslayer->items.count()).toString() + "</td></tr>";

	// Total Categories
	currentreporthtml += "<tr><td><b>Total Categories:</b></td><td>" + QVariant(this->currentbusinesslayer->categories.count()).toString() + "</td></tr>";

	// Total Locations
	currentreporthtml += "<tr><td><b>Total Locations:</b></td><td>" + QVariant(this->currentbusinesslayer->locations.count()).toString() + "</td></tr>";

	// Total Value
	double currentvalue = 0.00;
	while(itemvalueinterator.hasNext()) 
	{
        itemvalueinterator.next();
        item = itemvalueinterator.value();						
		
		currentvalue += (item->getQuantity() * item->getItemValue());
	}
	currentreporthtml += "<tr><td><b>Total Value:</b></td><td>" + this->currentbusinesslayer->toCurrencyString(currentvalue) + "</td></tr>";

	currentreporthtml += "</table></td></tr>";
	
	// Summary and data spacing
	currentreporthtml += "<tr bgcolor='LightGrey' width='100%'><td height='5'></td></tr>";

	// Create data
	currentreporthtml += "<tr><td><table width='100%' cellspacing='0' cellpadding='2'>";

	// Field headers
	currentreporthtml += "<tr><td><b><u>Item Name</b></u></td><td><b><u>Manufacturer</b></u></td><td><b><u>Category</b></u></td><td><b><u>Location</b></u></td><td><b><u>Quantity</b></u></td><td><b><u>Value</b></u></td></tr>";

    while(iteminterator.hasNext()) 
	{
        iteminterator.next();
        item = iteminterator.value();						
	
		currentreporthtml += "<tr bgcolor='" + rowcolor + "' bordercolor='" + rowcolor + "'><td>" + item->getName() + "</td><td>" + item->getManufacturer() + "</td><td>" + item->getCategory()->getName() + "</td><td>" + item->getLocation()->getName() + "</td><td>" + QVariant(item->getQuantity()).toString() + "</td><td>" + this->currentbusinesslayer->toCurrencyString(item->getItemValue()) + "</td></tr>";

		if(rowcolor == "LightGrey")
		{
			rowcolor = "White";
            qDebug(rowcolor.toLatin1());
		}
		else
		{
			rowcolor = "LightGrey";
            qDebug(rowcolor.toLatin1());
		}
    }	
	
	currentreporthtml += "</td></tr></table></center></body></html>";
}

void ReportDialog::ReportFullItemList()
{
	currentreporthtml = "";
	Item *currentitem = 0;
	QString rowcolor = "LightGrey";
	
	// Populate items
	Item *item = 0;
	QTreeWidgetItem *treeitem = 0;

	QHashIterator<int, Item*> iteminterator(this->currentbusinesslayer->items);
	QHashIterator<int, Item*> itemvalueinterator(this->currentbusinesslayer->items);

	currentreporthtml += "<html><head><title>Full Item List</title></head><body><center><table width='100%'>";

	// Create Summary
	currentreporthtml += "<tr><td><table width='100%' cellspacing='0' cellpadding='2'>";
	currentreporthtml += "<tr><td><b><u>Report Summary</u></b></td><td></td></tr>";

	// Report title
	currentreporthtml += "<tr><td><b>Report:</b></td><td>Full Item List</td></tr>";
	
	// Report Date
	currentreporthtml += "<tr><td><b>Report Date:</b></td><td>" + QDate::currentDate().toString() + "</td></tr>";

	// Total Items
	currentreporthtml += "<tr><td><b>Total Items:</b></td><td>" + QVariant(this->currentbusinesslayer->items.count()).toString() + "</td></tr>";

	// Total Categories
	currentreporthtml += "<tr><td><b>Total Categories:</b></td><td>" + QVariant(this->currentbusinesslayer->categories.count()).toString() + "</td></tr>";

	// Total Locations
	currentreporthtml += "<tr><td><b>Total Locations:</b></td><td>" + QVariant(this->currentbusinesslayer->locations.count()).toString() + "</td></tr>";

	// Total Value
	double currentvalue = 0.00;
	while(itemvalueinterator.hasNext()) 
	{
        itemvalueinterator.next();
        item = itemvalueinterator.value();						
		
		currentvalue += (item->getQuantity() * item->getItemValue());
	}
	currentreporthtml += "<tr><td><b>Total Value:</b></td><td>" + this->currentbusinesslayer->toCurrencyString(currentvalue) + "</td></tr>";

	currentreporthtml += "</table></td></tr>";
	
	// Summary and data spacing
	currentreporthtml += "<tr bgcolor='LightGrey' width='100%'><td height='5'></td></tr>";

	// Create data
	currentreporthtml += "<tr><td><table width='100%' cellspacing='0' cellpadding='2'>";

	// Field headers
	currentreporthtml += "<tr><td><b><u>Item Name</b></u></td><td><b><u>Manufacturer</b></u></td><td><b><u>Model</b></u></td><td><b><u>Category</b></u></td><td><b><u>Location</b></u></td><td><b><u>Serial Number</b></u></td><td><b><u>Date Acquired</b></u></td><td><b><u>Quantity</b></u></td><td><b><u>Value</b></u></td></tr>";

    while(iteminterator.hasNext()) 
	{
        iteminterator.next();
        item = iteminterator.value();						
	
		currentreporthtml += "<tr bgcolor='" + rowcolor + "' bordercolor='" + rowcolor + "'><td>" + item->getName() + "</td><td>" + item->getManufacturer() + "</td><td>" + item->getModel() + "</td><td>" + item->getCategory()->getName() + "</td><td>" + item->getLocation()->getName() + "</td><td>" + item->getSerialNumber() + "</td><td>" + item->getDateAcquired().toString("MM/dd/yyyy") + "</td><td>" + QVariant(item->getQuantity()).toString() + "</td><td>" + this->currentbusinesslayer->toCurrencyString(item->getItemValue()) + "</td></tr>";

		if(rowcolor == "LightGrey")
		{
			rowcolor = "White";
            qDebug(rowcolor.toLatin1());
		}
		else
		{
			rowcolor = "LightGrey";
            qDebug(rowcolor.toLatin1());
		}
    }	
	
	currentreporthtml += "</td></tr></table></center></body></html>";
}

void ReportDialog::ReportItemListWithImage()
{
	currentreporthtml = "";
	Item *currentitem = 0;
	QString rowcolor = "LightGrey";
	
	// Populate items
	Item *item = 0;
	QTreeWidgetItem *treeitem = 0;

	QHashIterator<int, Item*> iteminterator(this->currentbusinesslayer->items);
	QHashIterator<int, Item*> itemvalueinterator(this->currentbusinesslayer->items);

	currentreporthtml += "<html><head><title>Item List with Image</title></head><body><center><table width='100%'>";

	// Create Summary
	currentreporthtml += "<tr><td><table width='100%' cellspacing='0' cellpadding='2'>";
	currentreporthtml += "<tr><td><b><u>Report Summary</u></b></td><td></td></tr>";

	// Report title
	currentreporthtml += "<tr><td><b>Report:</b></td><td>Item List with Image</td></tr>";
	
	// Report Date
	currentreporthtml += "<tr><td><b>Report Date:</b></td><td>" + QDate::currentDate().toString() + "</td></tr>";

	// Total Items
	currentreporthtml += "<tr><td><b>Total Items:</b></td><td>" + QVariant(this->currentbusinesslayer->items.count()).toString() + "</td></tr>";

	// Total Categories
	currentreporthtml += "<tr><td><b>Total Categories:</b></td><td>" + QVariant(this->currentbusinesslayer->categories.count()).toString() + "</td></tr>";

	// Total Locations
	currentreporthtml += "<tr><td><b>Total Locations:</b></td><td>" + QVariant(this->currentbusinesslayer->locations.count()).toString() + "</td></tr>";

	// Total Value
	double currentvalue = 0.00;
	while(itemvalueinterator.hasNext()) 
	{
        itemvalueinterator.next();
        item = itemvalueinterator.value();						
		
		currentvalue += (item->getQuantity() * item->getItemValue());
	}
	currentreporthtml += "<tr><td><b>Total Value:</b></td><td>" + this->currentbusinesslayer->toCurrencyString(currentvalue) + "</td></tr>";

	currentreporthtml += "</table></td></tr>";
	
	// Summary and data spacing
	currentreporthtml += "<tr bgcolor='LightGrey' width='100%'><td height='5'></td></tr>";

    while(iteminterator.hasNext()) 
	{
        iteminterator.next();
        item = iteminterator.value();						
	
		// Create data
		currentreporthtml += "<tr><td><table width='100%' cellspacing='0' cellpadding='2'>";

		// Field headers
		currentreporthtml += "<tr><td><b>Item Name:</b></td><td>" + item->getName() + "</td><td><b>Serial Number:</b></td><td>" + item->getSerialNumber() + "</td></tr>";
		currentreporthtml += "<tr><td><b>Manufacturer:</b></td><td>" + item->getManufacturer() + "</td><td><b>Date Acquired:</b></td><td>" + item->getDateAcquired().toString("MM/dd/yyyy") + "</td></tr>";
		currentreporthtml += "<tr><td><b>Model:</b></td><td>" + item->getModel() + "</td><td><b>Quantity:</b></td><td>" + QVariant(item->getQuantity()).toString() + "</td></tr>";
		currentreporthtml += "<tr><td><b>Category:</b></td><td>" + item->getCategory()->getName() + "</td><td><b>Item Value:</b></td><td>" + this->currentbusinesslayer->toCurrencyString(item->getItemValue()) + "</td></tr>";
		currentreporthtml += "<tr><td><b>Location:</b></td><td>" + item->getLocation()->getName() + "</td><td></td><td></td></tr>";
		
		// Display Image
		if(item->getImageLocation() != "")
		{
			QImage itemimage(item->getImageLocation());
			QSize imagesize = this->currentbusinesslayer->scaleImageSize(itemimage.width(), itemimage.height(), 200, 200);			
			currentreporthtml += "<tr><td colspan='4'><img src='" + item->getImageLocation() + "' width='" + QVariant(imagesize.width()).toString() + "' height='" + QVariant(imagesize.height()).toString() + "'></td></tr>";
		}

		currentreporthtml += "</table></td></tr>";

		currentreporthtml += "<tr bgcolor='LightGrey' width='100%'><td height='5'></td></tr>";
    }	
	
	currentreporthtml += "</table></center></body></html>";
}

void ReportDialog::ReportItemsByCategory()
{
	currentreporthtml = "";
	Item *currentitem = 0;
	
	// Populate items
	Item *item = 0;
	Category *category = 0;

	QHashIterator<int, Item*> itemvalueinterator(this->currentbusinesslayer->items);	
	QHashIterator<int, Category*> categoryinterator(this->currentbusinesslayer->categories);

	currentreporthtml += "<html><head><title>Items By Category</title></head><body><center><table width='100%'>";

	// Create Summary
	currentreporthtml += "<tr><td><table width='100%' cellspacing='0' cellpadding='2'>";
	currentreporthtml += "<tr><td><b><u>Report Summary</u></b></td><td></td></tr>";

	// Report title
	currentreporthtml += "<tr><td><b>Report:</b></td><td>Items By Category</td></tr>";
	
	// Report Date
	currentreporthtml += "<tr><td><b>Report Date:</b></td><td>" + QDate::currentDate().toString() + "</td></tr>";

	// Total Items
	currentreporthtml += "<tr><td><b>Total Items:</b></td><td>" + QVariant(this->currentbusinesslayer->items.count()).toString() + "</td></tr>";

	// Total Categories
	currentreporthtml += "<tr><td><b>Total Categories:</b></td><td>" + QVariant(this->currentbusinesslayer->categories.count()).toString() + "</td></tr>";

	// Total Locations
	currentreporthtml += "<tr><td><b>Total Locations:</b></td><td>" + QVariant(this->currentbusinesslayer->locations.count()).toString() + "</td></tr>";

	// Total Value
	double currentvalue = 0.00;
	while(itemvalueinterator.hasNext()) 
	{
        itemvalueinterator.next();
        item = itemvalueinterator.value();						
		
		currentvalue += (item->getQuantity() * item->getItemValue());
	}
	currentreporthtml += "<tr><td><b>Total Value:</b></td><td>" + this->currentbusinesslayer->toCurrencyString(currentvalue) + "</td></tr>";
	currentreporthtml += "</table></td></tr>";
	
	// Summary and data spacing
	currentreporthtml += "<tr bgcolor='LightGrey' width='100%'><td height='5'></td></tr>";

	// Create data
	//currentreporthtml += "<tr><td><table width='100%' cellspacing='0' cellpadding='2'>";

    while(categoryinterator.hasNext()) 
	{
		QString rowcolor = "LightGrey";
		int categorycount = 0;
		double currentcategoryvalue = 0.00;

		QHashIterator<int, Item*> iteminterator(this->currentbusinesslayer->items);
		QHashIterator<int, Item*> categoryitemvalueinterator(this->currentbusinesslayer->items);

		categoryinterator.next();
        category = categoryinterator.value();					
	
		// Total Value and Count
		while(categoryitemvalueinterator.hasNext()) 
		{
			categoryitemvalueinterator.next();
			item = categoryitemvalueinterator.value();						
			
			if(item->getCategory()->getID() == category->getID())
			{
				categorycount++;
				currentcategoryvalue += (item->getQuantity() * item->getItemValue());
			}
		}

		// Create Category Summary
		currentreporthtml += "<tr><td><table width='100%' cellspacing='0' cellpadding='2'>";
		currentreporthtml += "<tr><td><b><u>Category Summary</u></b></td><td></td></tr>";

		// Report Date
		currentreporthtml += "<tr><td><b>Category Name:</b></td><td>" + category->getName() + "</td></tr>";

		// Total Items
		currentreporthtml += "<tr><td><b>Total Category Items:</b></td><td>" + QVariant(categorycount).toString() + "</td></tr>";

		
		currentreporthtml += "<tr><td><b>Total Category Value:</b></td><td>" + this->currentbusinesslayer->toCurrencyString(currentcategoryvalue) + "</td></tr>";

		currentreporthtml += "</table></td></tr>";

		// Create data
		currentreporthtml += "<tr><td><table width='100%' cellspacing='0' cellpadding='2'>";

		// Field headers
		currentreporthtml += "<tr><td><b><u>Item Name</b></u></td><td><b><u>Manufacturer</b></u></td><td><b><u>Category</b></u></td><td><b><u>Location</b></u></td><td><b><u>Quantity</b></u></td><td><b><u>Value</b></u></td></tr>";
		
		while(iteminterator.hasNext()) 
		{
			iteminterator.next();
			item = iteminterator.value();
		
			if(item->getCategory()->getID() == category->getID())
			{
				currentreporthtml += "<tr bgcolor='" + rowcolor + "' bordercolor='" + rowcolor + "'><td>" + item->getName() + "</td><td>" + item->getManufacturer() + "</td><td>" + item->getCategory()->getName() + "</td><td>" + item->getLocation()->getName() + "</td><td>" + QVariant(item->getQuantity()).toString() + "</td><td>" + this->currentbusinesslayer->toCurrencyString(item->getItemValue()) + "</td></tr>";

				if(rowcolor == "LightGrey")
				{
					rowcolor = "White";
                    qDebug(rowcolor.toLatin1());
				}
				else
				{
					rowcolor = "LightGrey";
                    qDebug(rowcolor.toLatin1());
				}
			}
		}
		currentreporthtml += "</table></td></tr>";
		// Summary and data spacing
		currentreporthtml += "<tr width='100%'><td><br><br></td></tr>";
    }	
	
	currentreporthtml += "</table></center></body></html>";
}

void ReportDialog::ReportItemsByLocation()
{
	currentreporthtml = "";
	Item *currentitem = 0;
	
	// Populate items
	Item *item = 0;
	Location *location = 0;

	QHashIterator<int, Item*> itemvalueinterator(this->currentbusinesslayer->items);	
	QHashIterator<int, Location*> locationinterator(this->currentbusinesslayer->locations);

	currentreporthtml += "<html><head><title>Items By Location</title></head><body><center><table width='100%'>";

	// Create Summary
	currentreporthtml += "<tr><td><table width='100%' cellspacing='0' cellpadding='2'>";
	currentreporthtml += "<tr><td><b><u>Report Summary</u></b></td><td></td></tr>";

	// Report title
	currentreporthtml += "<tr><td><b>Report:</b></td><td>Items By Location</td></tr>";
	
	// Report Date
	currentreporthtml += "<tr><td><b>Report Date:</b></td><td>" + QDate::currentDate().toString() + "</td></tr>";

	// Total Items
	currentreporthtml += "<tr><td><b>Total Items:</b></td><td>" + QVariant(this->currentbusinesslayer->items.count()).toString() + "</td></tr>";

	// Total Categories
	currentreporthtml += "<tr><td><b>Total Categories:</b></td><td>" + QVariant(this->currentbusinesslayer->categories.count()).toString() + "</td></tr>";

	// Total Locations
	currentreporthtml += "<tr><td><b>Total Locations:</b></td><td>" + QVariant(this->currentbusinesslayer->locations.count()).toString() + "</td></tr>";

	// Total Value
	double currentvalue = 0.00;
	while(itemvalueinterator.hasNext()) 
	{
        itemvalueinterator.next();
        item = itemvalueinterator.value();						
		
		currentvalue += (item->getQuantity() * item->getItemValue());
	}
	currentreporthtml += "<tr><td><b>Total Value:</b></td><td>" + this->currentbusinesslayer->toCurrencyString(currentvalue) + "</td></tr>";
	currentreporthtml += "</table></td></tr>";
	
	// Summary and data spacing
	currentreporthtml += "<tr bgcolor='LightGrey' width='100%'><td height='5'></td></tr>";

    while(locationinterator.hasNext()) 
	{
		QString rowcolor = "LightGrey";
		int locationcount = 0;
		double currentlocationvalue = 0.00;

		QHashIterator<int, Item*> iteminterator(this->currentbusinesslayer->items);
		QHashIterator<int, Item*> locationitemvalueinterator(this->currentbusinesslayer->items);

		locationinterator.next();
        location = locationinterator.value();					
	
		// Total Value and Count
		while(locationitemvalueinterator.hasNext()) 
		{
			locationitemvalueinterator.next();
			item = locationitemvalueinterator.value();						
			
			if(item->getLocation()->getID() == location->getID())
			{
				locationcount++;
				currentlocationvalue += (item->getQuantity() * item->getItemValue());
			}
		}

		// Create Location Summary
		currentreporthtml += "<tr><td><table width='100%' cellspacing='0' cellpadding='2'>";
		currentreporthtml += "<tr><td><b><u>Location Summary</u></b></td><td></td></tr>";

		// Report Date
		currentreporthtml += "<tr><td><b>Location Name:</b></td><td>" + location->getName() + "</td></tr>";

		// Total Items
		currentreporthtml += "<tr><td><b>Total Location Items:</b></td><td>" + QVariant(locationcount).toString() + "</td></tr>";

		
		currentreporthtml += "<tr><td><b>Total Location Value:</b></td><td>" + this->currentbusinesslayer->toCurrencyString(currentlocationvalue) + "</td></tr>";

		currentreporthtml += "</table></td></tr>";

		// Create data
		currentreporthtml += "<tr><td><table width='100%' cellspacing='0' cellpadding='2'>";

		// Field headers
		currentreporthtml += "<tr><td><b><u>Item Name</b></u></td><td><b><u>Manufacturer</b></u></td><td><b><u>Category</b></u></td><td><b><u>Location</b></u></td><td><b><u>Quantity</b></u></td><td><b><u>Value</b></u></td></tr>";
		
		while(iteminterator.hasNext()) 
		{
			iteminterator.next();
			item = iteminterator.value();
		
			if(item->getLocation()->getID() == location->getID())
			{
				currentreporthtml += "<tr bgcolor='" + rowcolor + "' bordercolor='" + rowcolor + "'><td>" + item->getName() + "</td><td>" + item->getManufacturer() + "</td><td>" + item->getCategory()->getName() + "</td><td>" + item->getLocation()->getName() + "</td><td>" + QVariant(item->getQuantity()).toString() + "</td><td>" + this->currentbusinesslayer->toCurrencyString(item->getItemValue()) + "</td></tr>";

				if(rowcolor == "LightGrey")
				{
					rowcolor = "White";
                    qDebug(rowcolor.toLatin1());
				}
				else
				{
					rowcolor = "LightGrey";
                    qDebug(rowcolor.toLatin1());
				}
			}
		}
		currentreporthtml += "</table></td></tr>";
		// Summary and data spacing
		currentreporthtml += "<tr width='100%'><td><br><br></td></tr>";
    }	
	
	currentreporthtml += "</table></center></body></html>";
}

void ReportDialog::printHtml(const QString &html)
{
	QPrintDialog printDialog(&printer, this);
	if(printDialog.exec())
	{
		QTextDocument textDocument;
		textDocument.setHtml(currentreporthtml);
		textDocument.print(&printer);
	}
}

void ReportDialog::on_reportsTreeWidget_itemClicked(QTreeWidgetItem*,int)
{
	this->GetReportSelected();
	this->LoadReport(currentreport);
}

void ReportDialog::on_printButton_clicked()
{
	this->printHtml(currentreporthtml);
}
