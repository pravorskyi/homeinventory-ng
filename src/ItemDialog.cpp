/*
 * ItemDialog.cpp - Item dialog
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "ItemDialog.h"

ItemDialog::ItemDialog(QWidget *parent, Qt::WindowFlags flags, BusinessLayer *businesslayer)
    : QDialog(parent, flags)
{
	currentbusinesslayer = businesslayer;
	currentitem = new Item();
	ui.setupUi(this);	

	ui.itemvalueLineEdit->setValidator(new QDoubleValidator(this));
	ui.clearimagePushButton->setEnabled(false);

	LoadData();
}

ItemDialog::~ItemDialog()
{

}

void ItemDialog::LoadData()
{
	// Populate category combobox
	ui.categoryComboBox->clear();

	Category *category = 0;
	
	QHashIterator<int, Category*> categoryinterator(this->currentbusinesslayer->categories);
	
    while(categoryinterator.hasNext()) 
	{
        categoryinterator.next();
        category = categoryinterator.value();						

		ui.categoryComboBox->addItem(category->getName(), category->getID());		
    }	

	// Populate location combobox
	ui.locationComboBox->clear();

	Location *location = 0;
	
	QHashIterator<int, Location*> locationinterator(this->currentbusinesslayer->locations);
	
    while(locationinterator.hasNext()) 
	{
        locationinterator.next();
        location = locationinterator.value();						

		ui.locationComboBox->addItem(location->getName(), location->getID());		
    }	

	ui.dateacquiredDateEdit->setDate(QDate::currentDate());
}

void ItemDialog::setCurrentItem(Item *item)
{
	currentitem = item;
	ui.itemnameLineEdit->setText(currentitem->getName());
	ui.manufacturerLineEdit->setText(currentitem->getManufacturer());
	ui.modelLineEdit->setText(currentitem->getModel());
	ui.categoryComboBox->setCurrentIndex(ui.categoryComboBox->findData(currentitem->getCategory()->getID()));
	ui.locationComboBox->setCurrentIndex(ui.locationComboBox->findData(currentitem->getLocation()->getID()));
	ui.serialnumberLineEdit->setText(currentitem->getSerialNumber());
	ui.dateacquiredDateEdit->setDate(currentitem->getDateAcquired());
	ui.quantitySpinBox->setValue(currentitem->getQuantity());
	ui.itemvalueLineEdit->setText(currentbusinesslayer->toDoubleString(currentitem->getItemValue()).replace(",", ""));

	if(currentitem->getImageLocation() != "")
	{
        qDebug("image:" + currentitem->getImageLocation().toLatin1());
		QImage itemimage(currentitem->getImageLocation());
		QSize imagesize = currentbusinesslayer->scaleImageSize(itemimage.width(), itemimage.height(), ui.itemimageTextBrowser->width() - 7, ui.itemimageTextBrowser->height() - 7);
		ui.itemimageTextBrowser->setText("<img src=\"" + currentitem->getImageLocation() + "\" width='" + QVariant(imagesize.width()).toString() + "' height='" + QVariant(imagesize.height()).toString() + "'>");
		ui.clearimagePushButton->setEnabled(true);
	}
	
}

void ItemDialog::setCurrentCategory(int categoryid)
{
	ui.categoryComboBox->setCurrentIndex(ui.categoryComboBox->findData(categoryid));
}

void ItemDialog::setCurrentLocation(int locationid)
{
	ui.locationComboBox->setCurrentIndex(ui.locationComboBox->findData(locationid));
}

void ItemDialog::on_saveButton_clicked()
{
	// Need to work on validation

	if(!ui.itemnameLineEdit->text().isEmpty())
	{
		currentitem->setName(ui.itemnameLineEdit->text());
		currentitem->setManufacturer(ui.manufacturerLineEdit->text());
		currentitem->setModel(ui.modelLineEdit->text());
		currentitem->setCategory(this->currentbusinesslayer->categories[ui.categoryComboBox->itemData(ui.categoryComboBox->currentIndex()).toInt()]);
		currentitem->setLocation(this->currentbusinesslayer->locations[ui.locationComboBox->itemData(ui.locationComboBox->currentIndex()).toInt()]);
		currentitem->setSerialNumber(ui.serialnumberLineEdit->text());
		currentitem->setDateAcquired(ui.dateacquiredDateEdit->date());
		currentitem->setQuantity(ui.quantitySpinBox->value());
		currentitem->setItemValue(ui.itemvalueLineEdit->text().toDouble());
		
		currentbusinesslayer->Save(currentitem);
		this->close();
	}
}	

void ItemDialog::on_cancelButton_clicked()
{
	this->close();
}

void ItemDialog::on_changeimageButton_clicked()
{
	QString fileName;
	QStringList fileNames;
	
	QFileDialog *fd = new QFileDialog(this, "Find image...", "c:\\");
    fd->setFileMode(QFileDialog::ExistingFile);	
	
	if (fd->exec() == QDialog::Accepted)
	{
        fileNames = fd->selectedFiles();

		QStringList::const_iterator i;
		
		for (i = fileNames.constBegin(); i != fileNames.constEnd(); ++i)
		{
			fileName = (*i);
		}
		
		currentitem->setImageLocation(fileName);

		QImage itemimage(fileName);
		QSize imagesize = currentbusinesslayer->scaleImageSize(itemimage.width(), itemimage.height(), ui.itemimageTextBrowser->width() - 7, ui.itemimageTextBrowser->height() - 7);
        qDebug(currentitem->getImageLocation().toLatin1());
		ui.itemimageTextBrowser->setHtml("<html><body><img src=\"" + currentitem->getImageLocation() + "\" width='" + QVariant(imagesize.width()).toString() + "' height='" + QVariant(imagesize.height()).toString() + "'></body></html>");		
		ui.clearimagePushButton->setEnabled(true);
	}	
	
	delete fd;
}

void ItemDialog::on_changedateButton_clicked()
{
	CalendarDialog calendardialog;
	calendardialog.setCurrentItem(currentitem);
	calendardialog.exec();
	ui.dateacquiredDateEdit->setDate(currentitem->getDateAcquired());
}

void ItemDialog::on_itemvalueLineEdit_textChanged(const QString &itemvalue)
{
	QString itemvaluestr = ui.itemvalueLineEdit->text();
	itemvaluestr.remove(".");
	itemvaluestr.insert((itemvaluestr.length() - 2), '.');
	ui.itemvalueLineEdit->setText(itemvaluestr);
	
}

void ItemDialog::on_clearimagePushButton_clicked()
{
	//ui.itemimageTextBrowser->setHtml("");
	currentitem->setImageLocation("");
	ui.clearimagePushButton->setEnabled(false);
}
