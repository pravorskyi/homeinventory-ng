#include "openfile.h"

#include <qcursor.h>
#ifdef Q_WS_WIN
#include <windows.h>
#endif

const bool hypocrite_org::OpenFile::open(QWidget *widget, const string 
filename) {
    bool retval = false;

    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
#ifdef Q_WS_X11
    Q_UNUSED(widget);
    // Assume we have KDE
    string s("kfmclient exec ");
    s += filename;
    retval = (system(s.c_str()) == 0);
#endif

#ifdef Q_WS_MACX
    // Running on a Mac in OS X
    Q_UNUSED(widget);
    string s("open ");
    s += filename;
    retval = (system(s.c_str()) == 0);
#endif

#ifdef Q_WS_WIN
    // Running in an MS Windows environment
	retval = (reinterpret_cast<int>(ShellExecute(widget->winId(), 
LPCWSTR("open"), LPCWSTR(filename.c_str()),
                            NULL, NULL, SW_SHOW)) > 32);
#endif

    QApplication::restoreOverrideCursor();
    return retval;
}
