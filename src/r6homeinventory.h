/*
 * r6homeinventory,h - Main window class
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef R6HOMEINVENTORY_H
#define R6HOMEINVENTORY_H

#include <QtWidgets/QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QSize>
#include <QHeaderView>
#include "ui_r6homeinventory.h"
#include "BusinessLayer.h"
#include "SystemLayer.h"
#include "CategoryDialog.h"
#include "LocationDialog.h"
#include "ItemDialog.h"
#include "ReportDialog.h"
#include "MoveItemsDialog.h"
#include "AboutDialog.h"

class R6HomeInventory : public QMainWindow
{
    Q_OBJECT

public:
    R6HomeInventory(QWidget *parent = 0, Qt::WindowFlags flags = 0);
    ~R6HomeInventory();

private:
    Ui::R6HomeInventoryClass ui;
	QString currenttitle;
	QString currentsection;
	SystemLayer *currentsystemlayer;
	BusinessLayer *currentbusinesslayer;
	Item *currentitem;
	Category *currentcategory;
	Location *currentlocation;
	QTreeWidgetItem *categoryroot;
	QTreeWidgetItem *locationroot;

private:
	void disableControls();
	void enableControls();
	void LoadData();
	void LoadSections();
	void LoadItems();
	void LoadItems(Category *category);
	void LoadItems(Location *location);
	void getSectionSelected();
	void setSectionSelected();
	void getItemSelected();
	void loadRecentFile();

private slots:
	void on_actionHelp_Contents_triggered();
	void on_actionAbout_R6_Home_Inventory_triggered();
	void on_itemsTreeWidget_customContextMenuRequested(const QPoint & pos);
	void on_sectionsTreeWidget_customContextMenuRequested(const QPoint & pos);
	void on_actionAll_Items_triggered();
	void on_actionReports_triggered();
	void on_itemsTreeWidget_itemDoubleClicked(QTreeWidgetItem*,int);
	void on_sectionsTreeWidget_itemDoubleClicked(QTreeWidgetItem*,int);
	void on_itemsTreeWidget_itemClicked(QTreeWidgetItem*,int);
	void on_sectionsTreeWidget_itemClicked(QTreeWidgetItem*,int);
	void on_actionDeleteItem_triggered();
	void on_actionEditItem_triggered();
	void on_actionDeleteLocation_triggered();
	void on_actionEditLocation_triggered();
	void on_actionDeleteCategory_triggered();
	void on_actionEditCategory_triggered();
	void on_actionAddItem_triggered();
	void on_actionAddLocation_triggered();
	void on_actionAddCategory_triggered();
	void on_actionExit_triggered();
	void on_actionOpen_triggered();
	void on_actionNew_triggered();
};

#endif // R6HOMEINVENTORY_H
