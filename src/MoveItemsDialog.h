/*
 * MoveItemsDialog.h - Move items dialog
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef MOVEITEMSDIALOG_H
#define MOVEITEMSDIALOG_H

#include <QDialog>
#include "ui_MoveItemsDialog.h"
#include "Item.h"
#include "BusinessLayer.h"
#include "Category.h"
#include "Location.h"

class MoveItemsDialog : public QDialog
{
    Q_OBJECT

public:
    MoveItemsDialog(QWidget *parent = 0, Qt::WindowFlags flags = 0);
    ~MoveItemsDialog();

private:
    Ui::MoveItemsDialogClass ui;
	BusinessLayer *currentbusinesslayer;
	Category *currentmovetocategory;
	Location *currentmovetolocation;
	QString currentsectiontype;

public:
	void setCurrentBusinessLayer(BusinessLayer *businesslayer);
	void setSectionType(QString sectiontype);
	void setMoveTo(Category *movetocategory);
	void setMoveTo(Location *movetolocation);
	int getMoveTo();

private slots:
	void on_cancelButton_clicked();
	void on_okButton_clicked();
};

#endif // MOVEITEMSDIALOG_H
