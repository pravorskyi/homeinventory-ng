/*
 * Category.cpp - Category class 
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "Category.h"

Category::Category(void)
{
	_id = 0;
	_name = "";
	_deleted = 0;
}

Category::Category(Category* category)
{
	this->setID(category->getID());
	this->setName(category->getName());
	this->setDeleted(category->getDeleted());
}

Category::~Category(void)
{
}

/****************************
	Set/Get for ID
*****************************/
void Category::setID(int id)
{
	_id = id;
}
int Category::getID()
{
	return _id;
}

/****************************
	Set/Get for Name
*****************************/
void Category::setName(QString name)
{
	_name = name;
}
QString Category::getName()
{
	return _name;
}

/****************************
	Set/Get for Deleted
*****************************/
void Category::setDeleted(bool deleted)
{
	_deleted = deleted;
}
bool Category::getDeleted()
{
	return _deleted;
}
