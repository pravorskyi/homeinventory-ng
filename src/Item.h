/*
 * Item.h - Item class 
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef ITEM_H
#define ITEM_H

#include <QObject>
#include <QString>
#include <QDate>
#include "Category.h"
#include "Location.h"

class Item
{

public:
	Item(void);
	Item(Item *item);
	~Item(void);


private:
	int _id;
	QString _name;
	QString _manufacturer;
	QString _model;
	Category *_category;
	Location *_location;
	QString _serialnumber;
	QDate _dateacquired;
	int _quantity;
	double _itemvalue;
	QString _imagelocation;
	bool _deleted;

public:
	void setID(int id);
	int getID();

	void setName(QString name);
	QString getName();

	void setManufacturer(QString manufacturer);
	QString getManufacturer();

	void setModel(QString model);
	QString getModel();

	void setCategory(Category *category);
	Category *getCategory();

	void setLocation(Location *location);
	Location *getLocation();

	void setSerialNumber(QString serialnumber);
	QString getSerialNumber();

	void setDateAcquired(QDate dateacquired);
	QDate getDateAcquired();

	void setQuantity(int quantity);
	int getQuantity();

	void setItemValue(double itemvalue);
	double getItemValue();

	void setImageLocation(QString imagelocation);
	QString getImageLocation();

	void setDeleted(bool deleted);
	bool getDeleted();
};

#endif // ITEM_H
