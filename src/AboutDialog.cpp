/*
 * AboutDialog.cpp - About dialog
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "AboutDialog.h"

AboutDialog::AboutDialog(QWidget *parent, Qt::WindowFlags flags)
    : QDialog(parent, flags)
{
	QString abouttext = "<html><body><center><h2>HomeInventory-NG 3.0-alpha</h2>";

	ui.setupUi(this);	
	this->setWindowTitle(tr("About ") + qApp->applicationName());
	abouttext += "QT Version: " + QVariant(qVersion()).toString() + "<br>";

	abouttext += "For support please use:<br>";
	abouttext += "Website: <a href='https://gitlab.com/pravorskyi/homeinventory-ng'>"
	             "https://gitlab.com/pravorskyi/homeinventory-ng</a><br><br>";
	abouttext += "&copy; <small>2006 R6 Software, LLC</small><br>"
	             "&copy; <small>2020 Andrii Pravorskyi</small><br>";
	abouttext += "</center></body></html>";

	ui.aboutTextBrowser->setHtml(abouttext);
}

AboutDialog::~AboutDialog()
{

}
