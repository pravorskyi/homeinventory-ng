/*
 * CategoryDialog.cpp - Category dialog
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "CategoryDialog.h"

CategoryDialog::CategoryDialog(QWidget *parent, Qt::WindowFlags flags, BusinessLayer *businesslayer)
    : QDialog(parent, flags)
{
	currentbusinesslayer = businesslayer;
	currentcategory = new Category();
	ui.setupUi(this);	
}

CategoryDialog::~CategoryDialog()
{

}

void CategoryDialog::setCurrentCategory(Category *category)
{
	currentcategory = category;
	ui.categorynameLineEdit->setText(currentcategory->getName());
}

void CategoryDialog::on_saveButton_clicked()
{
	if(!ui.categorynameLineEdit->text().isEmpty())
	{
		currentcategory->setName(ui.categorynameLineEdit->text());
		currentbusinesslayer->Save(currentcategory);
		this->close();
	}
}	

void CategoryDialog::on_cancelButton_clicked()
{
	this->close();
}
