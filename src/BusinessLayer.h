/*
 * BusinessLayer.h - Provides the business layer between the UI and database 
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef BUSINESSLAYER_H
#define BUSINESSLAYER_H

#include <QObject>
#include <QHash>
#include <QString>
#include <QtSql>
#include <QFile>
#include <QMessageBox>
#include <QLocale>
#include <iostream>

#include "Item.h"
#include "Category.h"
#include "Location.h"
#include "SystemLayer.h"

class BusinessLayer
{
public:
    BusinessLayer(SystemLayer *systemdb);
    ~BusinessLayer();

private:
	QSqlDatabase db;
	QString currentfilename;
	SystemLayer *systemlayer;

public:
    QHash<int, Item*> items;
	QHash<int, Category*> categories;
	QHash<int, Location*> locations;
	QList<Item*> currentitems;
		 
public:	
	bool CheckFile(const QString &filename);
	void setSystemLayer(SystemLayer *systemdb);
	int getDaysRemaining();
	bool isRegistered();
	bool saveRegistration(QString key1, QString key2, QString key3);
	bool hasItems(Category *category);
	bool hasItems(Location *location);
	bool moveItems(Category *fromcategory, Category *tocategory);
	bool moveItems(Location *fromlocation, Location *tolocation);
	bool deleteItems(Category *category);
	bool deleteItems(Location *location);

	bool LoadData();

	bool Save(Item *item);
	bool Save(Category *category);
	bool Save(Location *location);

	bool Delete(Item *item);
	bool Delete(Category *category);
	bool Delete(Location *location);

	QString toCurrencyString(double amount);
	QString toDoubleString(double amount);
	QSize scaleImageSize(int sourcewidth, int sourceheight, int destwidth, int destheight);

private:
	bool OpenDatabase(const QString &filename);	
	bool FileUpgrade(const QString &fileversion);
	bool CreateTables();
	bool ValidateDatabase(const QString &fileversion);
	QString FileVersion();
	int GetLastInsertID();
};

#endif // BUSINESSLAYER_H
