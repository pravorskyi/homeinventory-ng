/*
 * ItemDialog.h - Item dialog
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef ITEMDIALOG_H
#define ITEMDIALOG_H

#include <QDialog>
#include <QMessageBox>
#include <QFileDialog>
#include <QSize>
#include "ui_ItemDialog.h"
#include "BusinessLayer.h"
#include "Item.h"
#include "CalendarDialog.h"

class ItemDialog : public QDialog
{
    Q_OBJECT

public:
    ItemDialog(QWidget *parent = 0, Qt::WindowFlags flags = 0, BusinessLayer *businesslayer = 0);
    ~ItemDialog();

private:
    Ui::ItemDialogClass ui;
	BusinessLayer *currentbusinesslayer;
	Item *currentitem;
	QTextDocument itemimagedoc;

private:
	void LoadData();

public:
	void setCurrentItem(Item *item);
	void setCurrentCategory(int categoryid);
	void setCurrentLocation(int locationid);

private slots:
	void on_clearimagePushButton_clicked();
	void on_itemvalueLineEdit_textChanged(const QString &itemvalue);
	void on_changedateButton_clicked();
	void on_changeimageButton_clicked();
	void on_cancelButton_clicked();
	void on_saveButton_clicked();

};

#endif // ITEMDIALOG_H
