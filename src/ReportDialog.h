/*
 * ReportDialog.h - Report dialog
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef REPORTDIALOG_H
#define REPORTDIALOG_H

#include <QDialog>
#include <QMessageBox>
#include <QPrinter>
#include <QPrintDialog>
#include <QPainter>
#include <QTextDocument>

#include "ui_ReportDialog.h"
#include "BusinessLayer.h"

class ReportDialog : public QDialog
{
    Q_OBJECT

public:
    ReportDialog(QWidget *parent = 0, Qt::WindowFlags flags = 0, BusinessLayer *businesslayer = 0);
    ~ReportDialog();

private:
    Ui::ReportDialogClass ui;
	BusinessLayer *currentbusinesslayer;
	QString currentreport;
	QString currentreporthtml;
	QPrinter printer;

private:
	void ReportItemList();
	void ReportFullItemList();
	void ReportItemListWithImage();
	void ReportItemsByCategory();
	void ReportItemsByLocation();
	void LoadReports();
	void LoadReport(QString reportname);
	void GetReportSelected();
	void printHtml(const QString &html);

private slots:
	void on_printButton_clicked();
	void on_reportsTreeWidget_itemClicked(QTreeWidgetItem*,int);
	void on_cancelButton_clicked();
	void on_saveButton_clicked();

};

#endif // REPORTDIALOG_H
