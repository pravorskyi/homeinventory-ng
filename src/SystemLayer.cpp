/*
 * SystemLayer.cpp - System layer storing application wide settings 
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "SystemLayer.h"

SystemLayer::SystemLayer()
{
	// Call CheckFile()
	if(!CheckFile())
	{
		return;
	}
}

SystemLayer::~SystemLayer()
{
	// Close the database
	if(db.isOpen())
	{
		db.close();
	}
	QSqlDatabase::removeDatabase(db.connectionName());
}

void SystemLayer::setLastOpenedFile(QString filename)
{
	if(!db.isOpen())
	{
		CheckFile();	
	}

	QSqlQuery myquery(this->db);
		
	// Create the registration table		
	myquery.prepare("UPDATE recentfiles SET filename = :filename WHERE fileid = 1;");
	myquery.bindValue(":filename", filename);
	myquery.exec();

}

QString SystemLayer::getLastOpenedFile()
{
	if(!db.isOpen())
	{
		CheckFile();	
	}

	QSqlQuery q("SELECT filename FROM recentfiles WHERE fileid = 1");			
	while(q.next())
	{
		return q.value(0).toString();
        qDebug("Recent file: " + q.value(0).toString().toLatin1());
	}	
}


/****************************
	CheckFile is used to check if the SQLite, if it doesn't then it will
	create the database file.  Uses FileVersion to determine if the file is 
	current version.  It also uses a dialog to ask the user if they
	want to upgrade the file.
*****************************/
bool SystemLayer::CheckFile()
{
	QString fileversion;
	const QString filename = "system.db";

	// Check if files exists
	if(QFile::exists(filename))
	{				
		if(!OpenDatabase(filename))
		{
			return false;
		}
		
		fileversion = FileVersion();

		// Check the file version
		if(fileversion == "2.1")
		{			
			// Validate the file
			if(!ValidateDatabase(fileversion))
			{
				// Display a message box stating the file is not valid
				QMessageBox fileinvalidmb("R6 Home Inventory File Error",
                           "The system file you are trying to open is invalid.\n",
						   QMessageBox::Critical,
						   QMessageBox::Ok | QMessageBox::Default,0, 0);
				fileinvalidmb.exec();
				return false;
			}
			else
			{
				return true;
			}
		}		
	}
	else
	{		
		// Create the database and insert data
		if(!OpenDatabase(filename))
		{
			return false;
		}

		CreateTables();
	}

	return true;
}

/****************************
	OpenDatabase opens the SQLite database file and sets the db object
*****************************/
bool SystemLayer::OpenDatabase(const QString &filename)
{
	db = QSqlDatabase::addDatabase("QSQLITE");
	db.setDatabaseName(filename);

	if(!db.open())
	{		
		return false;
	}

	return true;
}

/****************************
	CreateTables creates all the tables for a new database file.  This function 
	also inserts the default data
*****************************/
bool SystemLayer::CreateTables()
{
	if(db.isOpen())
	{
		QSqlQuery myquery(this->db);
		
		// Create the registration table
		myquery.exec("CREATE TABLE reg( keyid int, key1 int, key2 int, key3 int, key4 int);");		
		myquery.exec("INSERT INTO reg(keyid, key4) VALUES (1, 0);");

		// Create the recentfiles table
		myquery.exec("CREATE TABLE recentfiles( fileid int, filename varchar(200));");		
		myquery.exec("INSERT INTO recentfiles(fileid, filename) VALUES (1, '');");

		// Create the filedata table
		myquery.exec("CREATE TABLE filedata( version varchar(10) );");		
		myquery.exec("INSERT INTO filedata( version ) VALUES ( '2.1' );");
		return true;
	}
	else
	{
		return false;
	}

	return false;
}

/****************************
	FileVersion checks the database file version and returns the current version string
*****************************/
QString SystemLayer::FileVersion()
{
	if(!db.isOpen())
	{
		CheckFile();	
	}

	// Check if db is open
	if(db.isOpen())
	{
		// Iterate through the tables to find out where the version is located
		QStringList tablelist = db.tables(QSql::Tables);				
		QStringList::const_iterator i;
		
		for (i = tablelist.constBegin(); i != tablelist.constEnd(); ++i)
		{			
            if(*i == "reg") // Version 1.0
			{								
				QSqlQuery q("SELECT version FROM reg");												
				while(q.next())
				{
					return q.value(0).toString();
				}								
			}
            else if(*i == "filedata") // Version 1.1
			{
				QSqlQuery q("SELECT version FROM filedata");												
				while(q.next())
				{
					return q.value(0).toString();
				}	
			}
		}				
	}

	return "unknown";
}

/****************************
	ValidateDatabase validates the database tables and fields to insure the file 
	is correct
*****************************/
bool SystemLayer::ValidateDatabase(const QString &fileversion)
{
	if(!db.isOpen())
	{
		CheckFile();	
	}

	if(fileversion == "1.2")
	{
		return true;
	}

	if(fileversion == "2.0")
    {
		return true;
	}
	
    if(fileversion == "2.1")
    {
		return true;
	}
	
	
	return false;
}

/****************************
	FileUpgrade upgrades the database file to the current version
*****************************/
bool SystemLayer::FileUpgrade(const QString &fileversion)
{
	if(!db.isOpen())
	{		
		return false;
	}

	if(fileversion == "1.2")
	{
		// Validate the file
		if(!ValidateDatabase(fileversion))
		{
			return false;
		}
		
		return true;
	}

	return false;
}

/****************************
	LoadData loads all the system data from the system database
*****************************/
bool SystemLayer::LoadData()
{

	return true;
}

/****************************
	UpgradeSQL updates the system.db through a sql query
	This will be used by the BusinessLayer to update the system database
*****************************/
bool SystemLayer::UpgradeSQL(const QString &sql)
{	
	if(db.isOpen())
	{
		QSqlQuery myquery(this->db);
		bool result = myquery.exec(sql);
		QVariant queryresult(result);
		
		return true;
	}
	else
	{	
		return false;
	}
}

