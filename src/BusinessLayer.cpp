/*
 * BusinessLayer.h - Provides the business layer between the UI and database 
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "BusinessLayer.h"
#include <QStringList>

BusinessLayer::BusinessLayer(SystemLayer *systemdb)
{
	systemlayer = systemdb;
}

BusinessLayer::~BusinessLayer()
{
}

/****************************
	setSystemLayer will set the current systemlayer for the business layer
*****************************/
void BusinessLayer::setSystemLayer(SystemLayer *systemdb)
{
	systemlayer = systemdb;
}

/****************************
	hasItems checks a category to see if it has items
****************************/
bool BusinessLayer::hasItems(Category *category)
{
	Item *item = 0;
	QHashIterator<int, Item*> iteminterator(this->items);
	
    while(iteminterator.hasNext()) 
	{
        iteminterator.next();
        item = iteminterator.value();						

		if(item->getCategory()->getID() == category->getID())
		{
			return true;
		}
    }	

	return false;
}

/****************************
	hasItems checks a location to see if it has items
****************************/
bool BusinessLayer::hasItems(Location *location)
{
	Item *item = 0;
	QHashIterator<int, Item*> iteminterator(this->items);
	
    while(iteminterator.hasNext()) 
	{
        iteminterator.next();
        item = iteminterator.value();						

		if(item->getLocation()->getID() == location->getID())
		{
			return true;
		}
    }	

	return false;
}

/****************************
	moveItems moves items from one category to another
****************************/
bool BusinessLayer::moveItems(Category *fromcategory, Category *tocategory)
{
	Item *item = 0;
	QHashIterator<int, Item*> iteminterator(this->items);
	
    while(iteminterator.hasNext()) 
	{
        iteminterator.next();
        item = iteminterator.value();						

		if(item->getCategory()->getID() == fromcategory->getID())
		{
			item->setCategory(tocategory);
			this->Save(item);
		}
    }	

	return true;
}

/****************************
	moveItems moves items from one location to another
****************************/
bool BusinessLayer::moveItems(Location *fromlocation, Location *tolocation)
{
	Item *item = 0;
	QHashIterator<int, Item*> iteminterator(this->items);
	
    while(iteminterator.hasNext()) 
	{
        iteminterator.next();
        item = iteminterator.value();						

		if(item->getLocation()->getID() == fromlocation->getID())
		{
			item->setLocation(tolocation);
			this->Save(item);
		}
    }	

	return true;
}

/****************************
	deleteItems deletes items from one category to another
****************************/
bool BusinessLayer::deleteItems(Category *category)
{
	Item *item = 0;
	QHashIterator<int, Item*> iteminterator(this->items);
	
    while(iteminterator.hasNext()) 
	{
        iteminterator.next();
        item = iteminterator.value();						

		if(item->getCategory()->getID() == category->getID())
		{
			this->Delete(item);
		}
    }	

	return true;
}

/****************************
	deleteItems moves items from one location to another
****************************/
bool BusinessLayer::deleteItems(Location *location)
{
	Item *item = 0;
	QHashIterator<int, Item*> iteminterator(this->items);
	
    while(iteminterator.hasNext()) 
	{
        iteminterator.next();
        item = iteminterator.value();						

		if(item->getLocation()->getID() == location->getID())
		{
			this->Delete(item);
		}
    }	

	return true;
}

/****************************
	CheckFile is used to check if the SQLite, if it doesn't then it will
	create the database file.  Uses FileVersion to determine if the file is 
	current version.  It also uses a dialog to ask the user if they
	want to upgrade the file.
*****************************/
bool BusinessLayer::CheckFile(const QString &filename)
{
	// Clear current data
	this->items.clear();
	this->categories.clear();
	this->locations.clear();

	QString fileversion;

	// Check if files exists
	if(QFile::exists(filename))
	{				
		if(!OpenDatabase(filename))
		{
			return false;
		}
		
		fileversion = FileVersion();

		// Check the file version
		if(fileversion == "2.1")
		{			
			// Validate the file
			if(!ValidateDatabase(fileversion))
			{
				// Display a message box stating the file is not valid
				QMessageBox fileinvalidmb("R6 Home Inventory File Error",
                           "The file you are trying to open is invalid.\n"
                           "Please check the file you are opening.",
						   QMessageBox::Critical,
						   QMessageBox::Ok | QMessageBox::Default,0, 0);
				fileinvalidmb.exec();
								
				if(CheckFile(currentfilename))
				{
					return true;
				}

				return false;
			}
			else
			{
				// File is valid
				currentfilename = filename;
				return true;
			}
		}
		else
		{
			// Display the file upgrade dialog
			QMessageBox fileupgrademb("R6 Home Inventory File Upgrade",
				"The file version (" + fileversion + ") you are opening is older than the current file version (2.1).\n"
                "Would you like to upgrade the file?",
				QMessageBox::Warning,
				QMessageBox::Yes | QMessageBox::Default,
				QMessageBox::No, 0);
            fileupgrademb.setButtonText(QMessageBox::Yes, "Upgrade");
            fileupgrademb.setButtonText(QMessageBox::No, "Cancel");

            switch(fileupgrademb.exec()) 
			{
				case QMessageBox::Yes:
					// Upgrade file
					if(!FileUpgrade(fileversion))
					{
						if(CheckFile(currentfilename))
						{
							return true;
						}

						return false;
					}
					else
					{
						currentfilename = filename;
						return true;
					}
					break;
				case QMessageBox::No:
					// Cancel upgrade
				    QMessageBox fileupgradecancelmb("R6 Home Inventory File Upgrade",
                           "You cannot open the file if you do not upgrade.\n"
                           "If you want to open the file please use R6 Home Inventory " + fileversion + ".",
						   QMessageBox::Information,
						   QMessageBox::Ok | QMessageBox::Default,0, 0);
					fileupgradecancelmb.exec();

					if(CheckFile(currentfilename))
					{
						return true;
					}

					return false;
					break;				
            }


		}
	}
	else
	{		
		// Create the database and insert data
		if(!OpenDatabase(filename))
		{
			return false;
		}

		CreateTables();

		currentfilename = filename;
	}

	return true;
}

/****************************
	OpenDatabase opens the SQLite database file and sets the db object
*****************************/
bool BusinessLayer::OpenDatabase(const QString &filename)
{
	db = QSqlDatabase::addDatabase("QSQLITE");
	db.setDatabaseName(filename);

	if(!db.open())
	{		
		return false;
	}

	return true;
}

/****************************
	CreateTables creates all the tables for a new database file.  This function 
	also inserts the default data
*****************************/
bool BusinessLayer::CreateTables()
{
	if(db.isOpen())
	{
		QSqlQuery myquery(this->db);

		// Create item table
		myquery.exec("CREATE TABLE item (id INTEGER PRIMARY KEY, name VARCHAR(100) NOT NULL, manufacturer VARCHAR(100), model VARCHAR(100), categoryid INTEGER, locationid INTEGER, serialnumber VARCHAR(100), dateacquired DATE, quantity INTEGER, itemvalue DECIMAL, imagelocation VARCHAR(200), deleted BOOL);");

		// Create category table
		myquery.exec("CREATE TABLE category (id INTEGER PRIMARY KEY, name VARCHAR(100) NOT NULL, deleted BOOL);");

		// Create location table
		myquery.exec("CREATE TABLE location (id INTEGER PRIMARY KEY, name VARCHAR(100) NOT NULL, deleted BOOL);");		

		// Create the filedata table
		myquery.exec("CREATE TABLE filedata( firstname varchar(50), lastname varchar(50), address varchar(200), version varchar(10) );");		
		myquery.exec("INSERT INTO filedata( firstname, lastname, address, version ) VALUES ( '', '', '', '2.1' );");
		return true;
	}
	else
	{
		return false;
	}

	return false;
}

/****************************
	FileVersion checks the database file version and returns the current version string
*****************************/
QString BusinessLayer::FileVersion()
{
	// Check if db is open
	if(db.isOpen())
	{
		// Iterate through the tables to find out where the version is located
		QStringList tablelist = db.tables(QSql::Tables);				
		QStringList::const_iterator i;
		
		for (i = tablelist.constBegin(); i != tablelist.constEnd(); ++i)
		{			
            if(*i == "reg") // Previous versions
			{								
				QSqlQuery q("SELECT version FROM reg");												
				while(q.next())
				{
					return q.value(0).toString();
				}								
			}
            else if(*i == "filedata") // Version 2.0
			{
				QSqlQuery q("SELECT version FROM filedata");												
				while(q.next())
				{
					return q.value(0).toString();
				}	
			}
		}				
	}

	return "unknown";
}




/****************************
	ValidateDatabase validates the database tables and fields to insure the file 
	is correct
*****************************/
bool BusinessLayer::ValidateDatabase(const QString &fileversion)
{
	if(fileversion == "1.0")
	{
		return true;
	}
	if(fileversion == "1.1")
	{
		return true;
	}
	if(fileversion == "1.2")
	{
		return true;
	}
    if(fileversion == "2.0")
    {
        return true;
    }
	if(fileversion == "2.1")
	{		
		// ValidCount is an integer that increments by one if something matches
		// If the ValidCount total does not match then the file is invalid

		int ValidCount = 0;

		// Check tables
		QStringList tablelist = db.tables(QSql::Tables);				
		QStringList::const_iterator i;
		
		for (i = tablelist.constBegin(); i != tablelist.constEnd(); ++i)
		{			
            if(*i == "item")
			{
				ValidCount++;

				// Check table bills columns
				QSqlQuery q("select * from item LIMIT 0, 0");
				QSqlRecord rec = q.record();				

				for (int i=0; i < rec.count(); i++) 
				{
					if (rec.fieldName(i) == "id")
					{
						ValidCount++;
					}
					if (rec.fieldName(i) == "name")
					{
						ValidCount++;
					}
					if (rec.fieldName(i) == "manufacturer")
					{
						ValidCount++;
					}
					if (rec.fieldName(i) == "model")
					{
						ValidCount++;
					}
					if (rec.fieldName(i) == "categoryid")
					{
						ValidCount++;
					}
					if (rec.fieldName(i) == "locationid")
					{
						ValidCount++;
					}
					if (rec.fieldName(i) == "serialnumber")
					{
						ValidCount++;
					}					
					if (rec.fieldName(i) == "dateacquired")
					{
						ValidCount++;
					}
					if (rec.fieldName(i) == "quantity")
					{
						ValidCount++;
					}
					if (rec.fieldName(i) == "itemvalue")
					{
						ValidCount++;
					}
					if (rec.fieldName(i) == "imagelocation")
					{
						ValidCount++;
					}
					if (rec.fieldName(i) == "deleted")
					{
						ValidCount++;
					}
				}			
			}
            if(*i == "category")
			{
				ValidCount++;

				// Check table recurring columns
				QSqlQuery q("select * from category LIMIT 0, 0");
				QSqlRecord rec = q.record();				

				for (int i=0; i < rec.count(); i++) 
				{
					if (rec.fieldName(i) == "id")
					{
						ValidCount++;
					}
					if (rec.fieldName(i) == "name")
					{
						ValidCount++;
					}
					if (rec.fieldName(i) == "deleted")
					{
						ValidCount++;
					}			
				}			
			}
            if(*i == "location")
			{
				ValidCount++;

				// Check table recurring columns
				QSqlQuery q("select * from location LIMIT 0, 0");
				QSqlRecord rec = q.record();				

				for (int i=0; i < rec.count(); i++) 
				{
					if (rec.fieldName(i) == "id")
					{
						ValidCount++;
					}
					if (rec.fieldName(i) == "name")
					{
						ValidCount++;
					}
					if (rec.fieldName(i) == "deleted")
					{
						ValidCount++;
					}			
				}			
			}
            if(*i == "filedata")
			{
				ValidCount++;

				// Check table filedata columns
				QSqlQuery q("select * from filedata LIMIT 0, 0");
				QSqlRecord rec = q.record();				

				for (int i=0; i < rec.count(); i++) 
				{					 
					if (rec.fieldName(i) == "firstname")
					{
						ValidCount++;
					}
					if (rec.fieldName(i) == "lastname")
					{
						ValidCount++;
					}
					if (rec.fieldName(i) == "address")
					{
						ValidCount++;
					}
					if (rec.fieldName(i) == "version")
					{
						ValidCount++;
					}
				}
			}
		}

		// Test if the file is valid based on ValidCount
		if(ValidCount == 26)
		{
			return true;
		}
		else
		{
			return false;
		}		
	}
	
	return false;
}

/**** Modify: remove the System Layer and validate database stuff *******/
/****************************
	FileUpgrade upgrades the database file to the current version
*****************************/
bool BusinessLayer::FileUpgrade(const QString &fileversion)
{
	if(!db.isOpen())
	{		
		return false;
	}
	
	if(fileversion == "1.2")
	{
		// Validate the file
		if(!ValidateDatabase(fileversion))
		{
			return false;
		}
		
		if(systemlayer)
		{
			QSqlQuery createquery(this->db);


			// --- Create tables
			
			// Create upgrade temp tables
			createquery.exec("CREATE TABLE categorylinks (categoriesid INTEGER, categoryid INTEGER);");
			createquery.exec("CREATE TABLE locationlinks (itemsid INTEGER, locationid INTEGER);");

			// Create item table
			createquery.exec("CREATE TABLE item (id INTEGER PRIMARY KEY, name VARCHAR(100) NOT NULL, manufacturer VARCHAR(100), model VARCHAR(100), categoryid INTEGER, locationid INTEGER, serialnumber VARCHAR(100), dateacquired DATE, quantity INTEGER, itemvalue DECIMAL, imagelocation VARCHAR(200), deleted BOOL);");

			// Create category table
			createquery.exec("CREATE TABLE category (id INTEGER PRIMARY KEY, name VARCHAR(100) NOT NULL, deleted BOOL);");

			// Create location table
			createquery.exec("CREATE TABLE location (id INTEGER PRIMARY KEY, name VARCHAR(100) NOT NULL, deleted BOOL);");		

			// Create the filedata table
			createquery.exec("CREATE TABLE filedata( firstname varchar(50), lastname varchar(50), address varchar(200), version varchar(10) );");		
			createquery.exec("INSERT INTO filedata( firstname, lastname, address, version ) VALUES ( '', '', '', '2.0' );");

			
			// --- Migrate data

			// Migrate category data
			QSqlQuery categoryquery(this->db);
			categoryquery.exec("SELECT * FROM categories");
			Category *category = 0;

			QSqlQuery categorylinkquery(this->db);
			
			while (categoryquery.next()) 
			{		
				category = new Category();
				category->setName(categoryquery.record().value("categoryname").toString());
				this->Save(category);

				categorylinkquery.prepare("INSERT INTO categorylinks (categoriesid, categoryid) VALUES (:categoriesid, :categoryid);");
				categorylinkquery.bindValue(":categoriesid", categoryquery.record().value("categoryid").toInt());
				categorylinkquery.bindValue(":categoryid", category->getID());				
				categorylinkquery.exec();	
			}

			// Migrate location data
			QStringList locationslist;

			QSqlQuery locationsquery(this->db);
			locationsquery.exec("SELECT * FROM items;");
			Location *location = 0;
			
			QSqlQuery locationlinksquery(this->db);

			while (locationsquery.next()) 
			{		
				if(locationslist.contains(locationsquery.record().value("location").toString()) == false && locationsquery.record().value("location").toString() != "")
				{
					location = new Location();
					locationslist.append(locationsquery.record().value("location").toString());
					location->setName(locationsquery.record().value("location").toString());
					this->Save(location);

					locationlinksquery.prepare("INSERT INTO locationlinks (itemsid, locationid) VALUES (:itemsid, :locationid);");
					locationlinksquery.bindValue(":itemsid", locationsquery.record().value("itemid").toInt());
					locationlinksquery.bindValue(":locationid", location->getID());				
					locationlinksquery.exec();
				}
			}
			
			LoadData();

			// Migrate item data
			QSqlQuery itemsquery(this->db);
			itemsquery.exec("SELECT * FROM items;");
			Item *item = 0;

			QSqlQuery linksquery(this->db);

			while (itemsquery.next()) 
			{	
				item = new Item();

				// Set Category
				linksquery.prepare("SELECT * FROM categorylinks WHERE categoriesid = :categoriesid;");
				linksquery.bindValue(":categoriesid", itemsquery.record().value("categoryid").toInt());
				linksquery.exec();

				while (linksquery.next()) 
				{
					item->setCategory(this->categories[linksquery.record().value("categoryid").toInt()]);
				}

				// Set Location
				linksquery.prepare("SELECT * FROM locationlinks WHERE itemsid = :itemsid;");
				linksquery.bindValue(":itemsid", itemsquery.record().value("itemid").toInt());
				linksquery.exec();
				
				while (linksquery.next()) 
				{
					item->setLocation(this->locations[linksquery.record().value("locationid").toInt()]);
				}

				// Set Item Name
				item->setName(itemsquery.record().value("itemname").toString());

				// Set Manufacturer
				item->setManufacturer(itemsquery.record().value("manufacturer").toString());

				// Set Model
				item->setModel(itemsquery.record().value("model").toString());

				// Set Serial Number
				item->setSerialNumber(itemsquery.record().value("isbn").toString());

				// Set Date Acquired
				QDate dateacquired(1970, 01, 01);
				item->setDateAcquired(dateacquired.addDays((itemsquery.record().value("dateacquired").toInt()/86400)));

                qDebug(item->getDateAcquired().toString().toLatin1());

				// Set Quantity
				item->setQuantity(itemsquery.record().value("quantity").toInt());

				// Set Item Value
				item->setItemValue(itemsquery.record().value("value").toDouble());

				// Set Image Location 
				item->setImageLocation("");

				this->Save(item);
			}


			// --- Remove old data

			QSqlQuery dropquery(this->db);
			dropquery.exec("DROP TABLE categorylinks;");
			dropquery.exec("DROP TABLE locationlinks;");
			dropquery.exec("DROP TABLE reg;");
			dropquery.exec("DROP TABLE items;");
			dropquery.exec("DROP TABLE categories;");
        }
		
		return true;
	}
	if(fileversion == "2.0")
    {
	    QSqlQuery query(this->db);
		query.exec("UPDATE filedata set version = '2.1';");
        return true; 
    }
	
	return false;
}

/****************************
	LoadData takes the days, determines the QDates and selects the bills
	within the date range
*****************************/
bool BusinessLayer::LoadData(void)
{
	// Load Categories
	if(categories.count() > 0)
	{
		categories.clear();
	}

	QSqlQuery categoryquery(this->db);
	categoryquery.exec("SELECT * FROM category WHERE deleted = 0");
	Category *category = 0;

	while (categoryquery.next()) 
	{		
		category = new Category();

		category->setID(categoryquery.record().value("id").toInt());
		category->setName(categoryquery.record().value("name").toString());
		category->setDeleted(categoryquery.record().value("deleted").toBool());

		categories[category->getID()] = category;
	}
	
	// Load Locations
	if(locations.count() > 0)
	{
		locations.clear();
	}

	QSqlQuery locationquery(this->db);
	locationquery.exec("SELECT * FROM location WHERE deleted = 0");
	Location *location = 0;

	while (locationquery.next()) 
	{		
		location = new Location();

		location->setID(locationquery.record().value("id").toInt());
		location->setName(locationquery.record().value("name").toString());
		location->setDeleted(locationquery.record().value("deleted").toBool());

		locations[location->getID()] = location;
	}

	// Load Items
	if(items.count() > 0)
	{
		items.clear();
	}

	QSqlQuery itemquery(this->db);
	itemquery.exec("SELECT * FROM item WHERE deleted = 0");
	Item *item = 0;

	while (itemquery.next()) 
	{		
		item = new Item();

		item->setID(itemquery.record().value("id").toInt());
		item->setName(itemquery.record().value("name").toString());
		item->setManufacturer(itemquery.record().value("manufacturer").toString());
		item->setModel(itemquery.record().value("model").toString());
		item->setCategory(categories[itemquery.record().value("categoryid").toInt()]);
		item->setLocation(locations[itemquery.record().value("locationid").toInt()]);
		item->setSerialNumber(itemquery.record().value("serialnumber").toString());
		item->setDateAcquired(itemquery.record().value("dateacquired").toDate());
		item->setQuantity(itemquery.record().value("quantity").toInt());
		item->setItemValue(itemquery.record().value("itemvalue").toDouble());
		item->setImageLocation(itemquery.record().value("imagelocation").toString());
		item->setDeleted(itemquery.record().value("deleted").toBool());

		items[item->getID()] = item;
	}
	
	return true;
}

/****************************
	Save takes a reference for an Item object and saves it to the
	Hash and SQLite database file
*****************************/
bool BusinessLayer::Save(Item *item)
{
	if(!db.isOpen())
	{
		CheckFile(currentfilename);	
	}

	QSqlQuery query(this->db);
			
	if(item->getID() != 0)
	{			
		query.prepare("UPDATE item SET name = :name, manufacturer = :manufacturer, model = :model, categoryid = :categoryid, locationid = :locationid, serialnumber = :serialnumber, dateacquired = :dateacquired, quantity = :quantity, itemvalue = :itemvalue, imagelocation = :imagelocation, deleted = :deleted WHERE id = :id;");
		query.bindValue(":id", item->getID());
		query.bindValue(":name", item->getName());
		query.bindValue(":manufacturer", item->getManufacturer());
		query.bindValue(":model", item->getModel());
		query.bindValue(":categoryid", item->getCategory()->getID());
		query.bindValue(":locationid", item->getLocation()->getID());
		query.bindValue(":serialnumber", item->getSerialNumber());
		query.bindValue(":dateacquired", item->getDateAcquired());
		query.bindValue(":quantity", item->getQuantity());
		query.bindValue(":itemvalue", item->getItemValue());
		query.bindValue(":imagelocation", item->getImageLocation());
		query.bindValue(":deleted", QVariant(item->getDeleted()).toInt());	
		query.exec();
	}
	else
	{
		query.prepare("INSERT INTO item(id, name, manufacturer, model, categoryid, locationid, serialnumber, dateacquired, quantity, itemvalue, imagelocation, deleted) VALUES (NULL, :name, :manufacturer, :model, :categoryid, :locationid, :serialnumber, :dateacquired, :quantity, :itemvalue, :imagelocation, :deleted);");
		query.bindValue(":name", item->getName());
		query.bindValue(":manufacturer", item->getManufacturer());
		query.bindValue(":model", item->getModel());
		query.bindValue(":categoryid", item->getCategory()->getID());
		query.bindValue(":locationid", item->getLocation()->getID());
		query.bindValue(":serialnumber", item->getSerialNumber());
		query.bindValue(":dateacquired", item->getDateAcquired());
		query.bindValue(":quantity", item->getQuantity());
		query.bindValue(":itemvalue", item->getItemValue());
		query.bindValue(":imagelocation", item->getImageLocation());
		query.bindValue(":deleted", QVariant(item->getDeleted()).toInt());		
        query.exec();	

		// Retrieve last id
		item->setID(GetLastInsertID());

		// Update items QHash		
		this->items.insert(item->getID(), item);
	}

	LoadData();

	return true;
}

/****************************
	Save takes a reference for an Category object and saves it to the
	Hash and SQLite database file
*****************************/
bool BusinessLayer::Save(Category *category)
{
	if(!db.isOpen())
	{
		CheckFile(currentfilename);	
	}

	QSqlQuery query(this->db);
		
	if(category->getID() != 0)
	{			
		query.prepare("UPDATE category SET name = :name, deleted = :deleted WHERE id = :id;");
		query.bindValue(":id", category->getID());
		query.bindValue(":name", category->getName());
		query.bindValue(":deleted", QVariant(category->getDeleted()).toInt());			
        query.exec();				

		QMapIterator<QString, QVariant> i(query.boundValues());
		while (i.hasNext()) 
		{
			i.next();
		}
	}
	else
	{
		query.prepare("INSERT INTO category(id, name, deleted) VALUES (NULL, :name, :deleted);");
		query.bindValue(":name", category->getName());
		query.bindValue(":deleted", QVariant(category->getDeleted()).toInt());		
        query.exec();	

		// Retrieve last id
		category->setID(GetLastInsertID());

		// Update categories QHash		
		this->categories.insert(category->getID(), category);

		QMapIterator<QString, QVariant> i(query.boundValues());
		while (i.hasNext()) 
		{
			i.next();
		}
	}

	LoadData();

	return true;
}

/****************************
	Save takes a reference for an Location object and saves it to the
	Hash and SQLite database file
*****************************/
bool BusinessLayer::Save(Location *location)
{
	if(!db.isOpen())
	{
		CheckFile(currentfilename);	
	}

	QSqlQuery query(this->db);
			
	if(location->getID() != 0)
	{			
		query.prepare("UPDATE location SET name = :name, deleted = :deleted WHERE id = :id;");
		query.bindValue(":id", location->getID());
		query.bindValue(":name", location->getName());
		query.bindValue(":deleted", QVariant(location->getDeleted()).toInt());			
        query.exec();				
	}
	else
	{
		query.prepare("INSERT INTO location(id, name, deleted) VALUES (NULL, :name, :deleted);");
		query.bindValue(":name", location->getName());
		query.bindValue(":deleted", QVariant(location->getDeleted()).toInt());		
        query.exec();	

		// Retrieve last id
		location->setID(GetLastInsertID());

		// Update locations QHash		
		this->locations.insert(location->getID(), location);
	}

	LoadData();

	return true;
}

/****************************
	Takes a reference for an item object and sets it deleted in the
	Hash and SQLite database file
*****************************/
bool BusinessLayer::Delete(Item *item)
{
	if(!db.isOpen())
	{
		CheckFile(currentfilename);	
	}
	// Need to set the deleted member value to 1

	// Remove from items QHash
	items.remove(item->getID());

	item->setDeleted(true);

	// Save item
	Save(item);

	LoadData();

	return true;
}

/****************************
	Takes a reference for a category object and sets it deleted in the
	Hash and SQLite database file
*****************************/
bool BusinessLayer::Delete(Category *category)
{
	if(!db.isOpen())
	{
		CheckFile(currentfilename);	
	}
	// Need to set the deleted member value to 1

	// Remove from categories QHash
	categories.remove(category->getID());

	category->setDeleted(true);

	// Save category
	Save(category);

	LoadData();

	return true;
}

/****************************
	Takes a reference for a location object and sets it deleted in the
	Hash and SQLite database file
*****************************/
bool BusinessLayer::Delete(Location *location)
{
	if(!db.isOpen())
	{
		CheckFile(currentfilename);	
	}
	// Need to set the deleted member value to 1

	// Remove from locations QHash
	locations.remove(location->getID());

	location->setDeleted(true);

	// Save location
	Save(location);

	LoadData();

	return true;
}

/****************************
	GetLastInsertID retrieves the last inserted id
*****************************/
int BusinessLayer::GetLastInsertID()
{	
	if(!db.isOpen())
	{
		CheckFile(currentfilename);	
	}

	int lastinsertid = 0;

	QSqlQuery query(this->db);
	query.exec("SELECT last_insert_rowid();");
	
	while (query.next()) 
	{
		lastinsertid = query.record().value(0).toInt();
	}

	return lastinsertid;
}

/****************************
	toCurrencyString converts the string into a currency formatted string
*****************************/
QString BusinessLayer::toCurrencyString(double amount)
{
	QLocale myLocale(QLocale::English, QLocale::UnitedStates);
	QLocale::setDefault(myLocale);	 
	QString str = QString("$%L1").arg(amount, 0, 'f', 2);
	return str;
}

/****************************
	toDoubleString converts the string into a currency formatted string
*****************************/
QString BusinessLayer::toDoubleString(double amount)
{
	QLocale myLocale(QLocale::English, QLocale::UnitedStates);
	QLocale::setDefault(myLocale);	 
	QString str = QString("%L1").arg(amount, 0, 'f', 2);
	return str;
}

/****************************
	getSizeScaled scales the image size based on the source height and width, then scales it to the destination height
	and width
*****************************/
QSize BusinessLayer::scaleImageSize(int sourcewidth, int sourceheight, int destwidth, int destheight)
{
	QSize newsize(sourcewidth, sourceheight);
	newsize.scale(destwidth, destheight, Qt::KeepAspectRatio);
	return newsize;
}
