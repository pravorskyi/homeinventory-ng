/*
 * SystemLayer.h - System layer storing application wide settings 
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef SYSTEMLAYER_H
#define SYSTEMLAYER_H

#include <QtSql>
#include <QFile>
#include <QMessageBox>

class SystemLayer
{
public:
	SystemLayer();
	~SystemLayer(void);

private:
	//TODO: 1. Create DB benchmark
	//TODO: 2. Do not keep a copy of the QSqlDatabase around as a member of a class.
	//From official docs:
	//	"Warning: It is highly recommended that you do not keep a copy of the QSqlDatabase around as a member of a class..."
	QSqlDatabase db;

public:	
	bool LoadData();
	bool UpgradeSQL(const QString &sql);
	void setLastOpenedFile(QString file);
	QString getLastOpenedFile();

private:
	bool OpenDatabase(const QString &filename);
	bool CheckFile();
	bool FileUpgrade(const QString &fileversion);
	bool CreateTables();	
	bool ValidateDatabase(const QString &fileversion);
	QString FileVersion();
};

#endif // SYSTEMLAYER_H
