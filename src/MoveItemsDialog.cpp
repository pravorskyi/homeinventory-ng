/*
 * MoveItemsDialog.cpp - Move items dialog
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "MoveItemsDialog.h"

MoveItemsDialog::MoveItemsDialog(QWidget *parent, Qt::WindowFlags flags)
    : QDialog(parent, flags)
{
	ui.setupUi(this);	

	currentbusinesslayer = 0;
}

MoveItemsDialog::~MoveItemsDialog()
{

}

void MoveItemsDialog::setCurrentBusinessLayer(BusinessLayer *businesslayer)
{
	currentbusinesslayer = businesslayer;
}

void MoveItemsDialog::setSectionType(QString sectiontype)
{
	currentsectiontype = sectiontype;

	if(sectiontype == "category")
	{
		Category *category = 0;
		
		QHashIterator<int, Category*> categoryinterator(this->currentbusinesslayer->categories);
		
		while(categoryinterator.hasNext()) 
		{
			categoryinterator.next();
			category = categoryinterator.value();						

			ui.movetoComboBox->addItem(category->getName(), category->getID());		
		}	
	}
	if(sectiontype == "location")
	{
		Location *location = 0;
		
		QHashIterator<int, Location*> locationinterator(this->currentbusinesslayer->locations);
		
		while(locationinterator.hasNext()) 
		{
			locationinterator.next();
			location = locationinterator.value();						

			ui.movetoComboBox->addItem(location->getName(), location->getID());	
		}	
	}
}

void MoveItemsDialog::setMoveTo(Category *movetocategory)
{
	currentmovetocategory = movetocategory;
}

void MoveItemsDialog::setMoveTo(Location *movetolocation)
{
	currentmovetolocation = movetolocation;
}

int MoveItemsDialog::getMoveTo()
{
	return ui.movetoComboBox->itemData(ui.movetoComboBox->currentIndex()).toInt();
}

void MoveItemsDialog::on_okButton_clicked()
{
	
}

void MoveItemsDialog::on_cancelButton_clicked()
{

}
