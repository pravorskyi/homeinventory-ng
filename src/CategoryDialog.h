/*
 * CategoryDialog.h - Category dialog
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef CATEGORYDIALOG_H
#define CATEGORYDIALOG_H

#include <QDialog>
#include <QMessageBox>
#include "ui_CategoryDialog.h"
#include "BusinessLayer.h"

class CategoryDialog : public QDialog
{
    Q_OBJECT

public:
    CategoryDialog(QWidget *parent = 0, Qt::WindowFlags flags = 0, BusinessLayer *businesslayer = 0);
    ~CategoryDialog();

private:
    Ui::CategoryDialogClass ui;
	BusinessLayer *currentbusinesslayer;
	Category *currentcategory;

private:

public:
	void setCurrentCategory(Category *category);

private slots:
	void on_cancelButton_clicked();
	void on_saveButton_clicked();

};

#endif // CATEGORYDIALOG_H
