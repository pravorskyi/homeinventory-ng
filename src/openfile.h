#ifndef OPENFILE_H
#define OPENFILE_H

#include <qapplication.h>
#include <QWidget>

#ifdef Q_WS_X11
#include <cstdlib>
#endif

#ifdef Q_WS_MACX
#include <cstdlib>
#include <qmessagebox.h>
#endif

#include <string>

using namespace std;

namespace hypocrite_org {

/*!
 * \class OpenFile
 * \version $Revision: 1.4 $
 * \brief Open a given file using the underlying operating
 * system's default method.  We would typically use this
 * to open a URL using the default browser, for example.
 * Its use is undefined if the operating system does not
 * know how to handle the file we pass it.
 */
class OpenFile {
public:
    static const bool open(QWidget *widget, const string filename);
};
}

#endif