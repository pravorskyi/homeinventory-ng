/*
 * Item.cpp - Item class 
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "Item.h"

Item::Item(void)
{
	_id = 0;
	_name = "";
	_manufacturer = "";
	_model = "";
	_category = 0;
	_location = 0;
	_serialnumber = "";	
	_quantity = 0;
	_itemvalue = 0.00;
	_imagelocation = "";
	_deleted = 0;
}

Item::Item(Item *item)
{
	this->setID(item->getID());
	this->setName(item->getName());
	this->setManufacturer(item->getManufacturer());
	this->setModel(item->getModel());
	this->setCategory(item->getCategory());
	this->setLocation(item->getLocation());
	this->setSerialNumber(item->getSerialNumber());
	this->setDateAcquired(item->getDateAcquired());
	this->setQuantity(item->getQuantity());
	this->setItemValue(item->getItemValue());
	this->setImageLocation(item->getImageLocation());
	this->setDeleted(item->getDeleted());
}

Item::~Item(void)
{
}

/****************************
	Set/Get for ID
*****************************/
void Item::setID(int id)
{
	_id = id;
}
int Item::getID()
{
	return _id;
}

/****************************
	Set/Get for Name
*****************************/
void Item::setName(QString name)
{
	_name = name;
}
QString Item::getName()
{
	return _name;
}

/****************************
	Set/Get for Manufacturer
*****************************/
void Item::setManufacturer(QString manufacturer)
{
	_manufacturer = manufacturer;
}
QString Item::getManufacturer()
{
	return _manufacturer;
}

/****************************
	Set/Get for Model
*****************************/
void Item::setModel(QString model)
{
	_model = model;
}
QString Item::getModel()
{
	return _model;
}

/****************************
	Set/Get for Category
*****************************/
void Item::setCategory(Category *category)
{
	_category = category;
}
Category *Item::getCategory()
{
	return _category;
}

/****************************
	Set/Get for Location
*****************************/
void Item::setLocation(Location *location)
{
	_location = location;
}
Location *Item::getLocation()
{
	return _location;
}

/****************************
	Set/Get for Serial Number
*****************************/
void Item::setSerialNumber(QString serialnumber)
{
	_serialnumber = serialnumber;
}
QString Item::getSerialNumber()
{
	return _serialnumber;
}

/****************************
	Set/Get for Date Acquired
*****************************/
void Item::setDateAcquired(QDate dateacquired)
{
	_dateacquired = dateacquired;
}
QDate Item::getDateAcquired()
{
	return _dateacquired;
}

/****************************
	Set/Get for Quantity
*****************************/
void Item::setQuantity(int quantity)
{
	_quantity = quantity;
}
int Item::getQuantity()
{
	return _quantity;
}

/****************************
	Set/Get for Item Value
*****************************/
void Item::setItemValue(double itemvalue)
{
	_itemvalue = itemvalue;
}
double Item::getItemValue()
{
	return _itemvalue;
}

/****************************
	Set/Get for Image Location
*****************************/
void Item::setImageLocation(QString imagelocation)
{
	_imagelocation = imagelocation;
}
QString Item::getImageLocation()
{
	return _imagelocation;
}

/****************************
	Set/Get for Deleted
*****************************/
void Item::setDeleted(bool deleted)
{
	_deleted = deleted;
}
bool Item::getDeleted()
{
	return _deleted;
}
