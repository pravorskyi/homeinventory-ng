#include <QtTest/QtTest>
#include "../SystemLayer.h"

class TestSystemLayer: public QObject
{
    Q_OBJECT
private slots:
    void testDestructor();
};

void TestSystemLayer::testDestructor()
{
    // No database connections at start
    QCOMPARE(QSqlDatabase::connectionNames().count(), 0);

    auto * sl = new SystemLayer();
    QCOMPARE(QSqlDatabase::connectionNames().count(), 1);
    delete sl;

    // All database connections should be removed after destructor call
    QCOMPARE(QSqlDatabase::connectionNames().count(), 0);
}

QTEST_MAIN(TestSystemLayer)
#include "TestSystemLayer.moc"
