/*
 * Location.cpp - Location class
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "Location.h"

Location::Location(void)
{
	_id = 0;
	_name = "";
	_deleted = 0;
}

Location::Location(Location *location)
{
	this->setID(location->getID());
	this->setName(location->getName());
	this->setDeleted(location->getDeleted());
}

Location::~Location(void)
{
}

/****************************
	Set/Get for ID
*****************************/
void Location::setID(int id)
{
	_id = id;
}
int Location::getID()
{
	return _id;
}

/****************************
	Set/Get for Name
*****************************/
void Location::setName(QString name)
{
	_name = name;
}
QString Location::getName()
{
	return _name;
}

/****************************
	Set/Get for Deleted
*****************************/
void Location::setDeleted(bool deleted)
{
	_deleted = deleted;
}
bool Location::getDeleted()
{
	return _deleted;
}
