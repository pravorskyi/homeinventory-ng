/*
 * CalendarDialog.cpp - Calendar dialog used to select dates 
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "CalendarDialog.h"

CalendarDialog::CalendarDialog(QWidget *parent, Qt::WindowFlags flags)
    : QDialog(parent, flags)
{
	ui.setupUi(this);	
}

CalendarDialog::~CalendarDialog()
{

}

void CalendarDialog::setCurrentItem(Item *item)
{
	currentitem = item;	
	ui.dateCalendarWidget->setSelectedDate(currentitem->getDateAcquired());
}

void CalendarDialog::on_dateCalendarWidget_selectionChanged()
{
	currentitem->setDateAcquired(ui.dateCalendarWidget->selectedDate());
	this->close();
}
