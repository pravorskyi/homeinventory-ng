/*
 * Location.h - Location class
 * Copyright (C) 2007  Chris Roland
 *    
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *         
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *              
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef LOCATION_H
#define LOCATION_H

#include <QObject>
#include <QString>
#include <QDate>

class Location
{
public:
	Location(void);
	Location(Location* location);
	~Location(void);

private:
	int _id;
	QString _name;
	bool _deleted;

public:
	void setID(int id);
	int getID();

	void setName(QString name);
	QString getName();

	void setDeleted(bool deleted);
	bool getDeleted();
};

#endif // LOCATION_H
